第百五十九話 三年後，鮑邁斯特邊境伯暗殺計劃


「父親大人，瞑想做完了」

「大家都做完了嗎？」

「是」

「那麼，今天我們就來做些別的練習吧」

魔族襲來已經過去了三年，鮑邁斯特邊境伯領今天也是活力四溢。

政府之間的交涉到現在也還沒有進展。

我覺得能這樣在某種意義上也蠻厲害的，也許是現實超出了上面的人的預想吧。

被魔族之國當做垃圾的古舊魔道具修理完畢後大量流入了琳蓋亞大陸，靠著這些東西推進開發的結果，就是赫爾姆特王國、阿卡特神聖帝國的開發熱潮一直持續著。

平價的魔道具。

在魔族之國那邊，收購這些大型垃圾或賤價到等於白扔的舊魔道具根本花不了幾個錢。

一部分眼光敏銳的魔族，已經靠販賣中古魔道具積累了很多財富。

兩國間距離遙遠所以這些魔族大多很辛苦，不過那些為了成功不怕苦的年輕人都獲得了相當的收益。

雖然現在三國之間仍未定下貨幣兌換匯率，但只要用黃金、白金、寶石、可以換成錢的魔物素材、礦物之類的東西進行交易就沒有問題了。

對這種半封鎖半開放的貿易狀況有怨言的人很多，這些人在背地里也對交涉施加了很多壓力。

三個國家的政府對此則都保持了只要不偷運武器和奴隸就默認的立場。

因為顧忌這麼大量輸入魔道具……可能會觸及到自己的市場被奪的魔道具工會的敏感神經，所以私下裡制定了只進口魔道具工會做不出的商品這種秘密協定……這個共識現在在三國中已經非常普及。

琳蓋亞大陸的魔道具工會現在還造不出的車輛、重型機械類魔道具在開墾、道路工程、建設方面都非常活躍。大量在魔族之國已經基本沒有用途的帆船型魔導飛船也在天上飛來飛去，讓王國的交通網得到了強化。

鮑邁斯特邊境伯領也靠著從魔王大人的會社購入的魔道具進行著高速開發。

合併進來的南方諸島和秋津洲島的開發也很順利，我從萊拉小姐那裡購入的中小型魔導飛行船在領地內來回奔波。

王國和帝國的大型魔導飛行船數量也是大幅增加。

這些船全都是靠和魔族的私下貿易購入的。

因為沒有配備武器，這種舊式魔導飛行船根本不是魔族之國警備隊配備的新型艦船的對手。

是些只能拿來運貨，大多數被拋棄在船庫里落土或被當做大型垃圾扔著不管的船。

按魔王大人的說法，雖然眼光敏銳的魔族可以賣這個賺錢，但在一般魔族看來這些船就只是大型垃圾而已。

總之，我家定期的從魔王大人和萊拉小姐那裡購買魔道具，然後將其投入領地開發中不斷獲取成果。

我們這些魔法使也仍在用魔法協助開發，偶爾會作為冒險者出去活動一下。

在這樣的日子裡埃莉絲她們又再次生下了我的孩子，我則開始教已經三歲了的最早出生的弗里德里希這幾個孩子魔法。

也就是弗里德里希、安娜、艾爾莎、凱茵、布羅拉、伊蕾涅、比爾達、拉烏拉這八個孩子。

他們都還小，所以我真的只是教教基礎而已。

教會他們要每天做瞑想后，今天我打算換一個稍微有點游戲性的教學方法。

畢竟都還是小孩子，採取比較快樂的遊戲式教學比較好呢。

「大家各自挑選一艘自己喜歡的」

「哇啊，是魔導飛行船」

這是我委託鮑邁斯堡的玩具店製作的玩具魔導飛行船。

我把這些玩具拿給參加魔法鍛鍊的八個孩子，告訴他們每人選一艘自己喜歡的船。

「我，要這艘」

「我這艘」

「這艘吧？」

每個人都選好船後，我開始傳授他們操作的方法。

「這是做的很輕的模型。所以可以像這樣用『念力』驅動哦」

我拿過弗里德里希選中的船，像玩遙控一樣用魔法讓它動起來。

玩具穿像真正的魔導飛行船那樣在空中上下翻飛，速度也時快時慢，前進方向上如果有樹枝之類的障礙就躲開。簡單來說讓這個玩具船隨自己的意願移動就是這次訓練的內容。

「「「「「「「「父親大人，好厲害！」」」」」」」」

「只要每天好好鍛鍊，做到這樣很輕鬆哦。到時大家就來比賽吧？」

「好———！我要得第一」

「我才要得第一」

「是我」

「優勝者是我啦！」

弗里德里希他們立刻開始用魔法讓玩具船飛起來。

「啊咧？掉下去了」

「好奇怪？」

「凱茵，芙洛拉，這個意外的很難吧？」

卡特莉娜的兒子凱茵和卡琪婭的女兒希爾達的玩具船一上來就墜落了。

其實這個玩具船的構造有點特殊，如果不讓魔力均勻的纏繞在上面就會馬上墜落。

「弗里德里希你們的魔力很多。但是只是那樣是沒法使用魔法的哦。所以要從現在開始好好記住控制魔力的方法」

「那樣的話，就能像父親大人那樣了？」

「可以哦」

弗里德里希他們現在已經擁有和師傅相遇前的我那種程度的魔力和魔法才能了。

因為每天都有讓他們做瞑想，所以我覺得現在比起勉強增加魔力量還是應該更重視對魔力控制的教育。

未成熟的小孩子的魔法有突然爆發的危險。

不用焦慮，讓大家慢慢的學會魔法就行了。

「我會加油」

「浮起來了！」

弗里德里希他們很開心的享受著讓玩具魔導飛行船漂浮的訓練。

「很好很好」

「哎呀？氣氛相當熱烈呢，當家大人。羅德里希先生正在等你」

接下來的事就交給保姆們和護衛吧，你還有其他的工作，來接我的艾茉莉嫂子這麼說道。

「我還是不習慣被艾茉莉嫂子叫當家大人……」

「這也是沒辦法的呢。我也不習慣被周圍的人當做夫人們一樣的人對待啊」

雖然之前艾茉莉嫂子還被旁人視為侍女長，但自打去年她為我生下一個女兒後，家臣們似乎就把她視為和我的妻子們同等的人物了。

表面身份這種東西，鮑邁斯特邊境伯家的家臣們根本不會怎麼在意。

重要的是艾茉莉嫂子生下了我的女兒這個事實。

現在，我這個取名洛西娜的女兒正十分健康的成長著。

「那麼，羅德裡希有什麼事？」

「說是有話要對您說」

「他又要說教啊……」

我應該也有好好做貴族的工作吧。

雖然我的確偶爾會和艾爾、布蘭塔克先生、導師他們偷偷跑去魔之森狩獵啦。

或是用『和貴族會面？我為了教弗里德里希他們魔法正忙著呢！』的理由拒絕過他。

另外羅德里希嚴選出來的文件我的確都不怎麼細看就在上面簽字，可那不是也沒造成什麼糟糕的影響麼。

「我覺得文件不好好看清楚是不行的哦」

「這個嘛，這裡面包含著聽者落淚言者傷心的隱情啦」

「是嗎？」

「誒誒……」

當然，其實只是我覺得麻煩而已。

「艾茉莉大人，請不要被當家大人的謊話騙了」

「不是謊話哦」

是已經等不耐煩了麼？

叫我過去的羅德里希自己出現了。

「我是很忙的，所以工作得按順序來才行。羅德里希是又有些文件需要在明天之前讓我簽字了吧？」

「誒誒」

「其實把日期延長到一周左右也不要緊」

「可以嗎？」

反正有工作完美的羅德里希在，我認為就算晚一周在文件上簽字也沒問題。

「再怎麼說拖一周也是不行的！最多也就是三～四日！」

羅德里希雖然嘴上那麼說，但其實他還留有相當的周轉期。

這就好比小說家即便被編輯指定了截稿日，但其實編輯手中還有第二甚至第三最終截稿日是一個道理。

「總之就是這麼回事」

「看文件簽字這種程度的工作，請在期限內好好完成。當家大人今後還要生很多孩子，並作為鮑邁斯特邊境伯家的家主繼續精進才行啊……」

「雖然孩子已經生了一大堆呢」

的確，現在的鮑邁斯特邊境伯家正處於究極的嬰兒出生高峰期熱潮中。

這三年來，埃莉絲生了女兒瑪露蒂，伊娜生了兒子拉爾斯，露易絲生了兒子奧拉菲，維爾瑪生了兒子普恩爾德，卡特莉娜生了兒子耶格爾，泰蕾莎生了兒子迪爾庫，卡琪婭生了兒子艾崗，麗莎生了兒子尼古拉斯。

新嫁進來的阿格妮絲也生了女兒瑪利亞，辛迪生了女兒瓦涅莎，貝蒂生了女兒佩蘭。

秋津洲組那邊，涼子生了兒子涼介，雪生了女兒文子，唯生了兒子久通。

除埃莉絲和艾茉莉嫂子外的妻子們大多生的是兒子，羅德里希很開心的說如此一來就能創建鮑麥斯特邊境伯家分家了。

此外，還有大量家裡只有女孩的貴族家跑來懇求讓我兒子入贅過去當繼承人女婿，弄得我都要哭了。

我的兒子又不是拿來競拍的小牛……。

『請生更多孩子！』

然而，羅德里希卻一點都不顧及我的心情。

這傢伙明明和艾爾一樣也當了父親。

『艾爾文和鄙人的孩子，與當家大人孩子的立場是不同的』

對了，艾爾又和遙生了個女兒，這次他們為孩子取了紗織這個瑞穗風格的名字。

蕾婭也生下了名為格蘭塔的兒子，安娜則生了名為莫妮卡的女兒，這下艾爾也有四個孩子了。

羅德里希現在也成了兩個兒子一個女兒的父親，其他家臣們也不斷生下小孩。

因為孩子實在太多，弄的女性陣容的所有人都非常忙碌。

為了提高效率，鮑麥斯特邊境伯家的孩子和主要家臣的孩子被聚集到一起養育，由各家的妻子們輪流分擔著照看。

連家臣親人當中的未婚女孩也幾乎都被強制徵集來照顧嬰兒們了。

這裡面也有女孩們有為將來自己嫁人產子時做預習的理由。

不過，這其中擁有魔力的就只有我家的孩子們，所以我為他們專門準備魔法特訓的培育環節。

至於其他的基礎教育，因為每個人單獨教太麻煩了，我打算今後把他們全送去學校學習那些知識。

再說有點競爭的話他們應該也能學的更快些。

學校老師我打算請具備一定教養的高齡王都貴族來擔任。

總之，現在的鮑麥斯特邊境伯家因為各種原因變得非常熱鬧。

「所以說，我不是有好好做貴族的工作嗎。最早和魔王大人進行私人貿易的可是我哦」

我是第一個出手和魔族進行私人貿易的人。

因為反正很早以前就認識了魔王大人，所以我覺得這樣的關係蠻不錯的。

畢竟，現在的魔王大人已經成為了一家大會社的會長大人。

魔王大人的會社靠賣給我家各種東西得到的資金順利擴大了規模。

現在，這家會社已經作為和赫爾姆特王家、鮑麥斯特邊境伯家、布雷希洛德邊境伯家、布洛瓦邊境伯家做生意的貿易會社而變得非常有名。

雖然魔族中也有吵嚷著『被庶民彈壓的王家末裔為了再次實行獨裁統治，將我等貴重的資產和技術出賣給了外國！』的傢伙，但輿論基本上沒怎麼對魔王大人進行批判。

畢竟，魔王大人最開始賣給我們的中古魔道具原本只是大型垃圾，或是留下了也只有扔掉一途的誰都不會去買的古舊產品。

至於魔導飛行船就更是如此了——全都是些不想繳納處理費的人擅自拋棄的船只，對當拋棄地點地住民來說只不過是堆添麻煩的東西罷了。

這些船連解體都要花很多錢。

這樣的麻煩可以一口氣收拾乾淨，是不會有人對此抱怨的。

魔王大人的會社如今仍在廢置區撿拾被扔掉的魔道具或魔導飛行船、以及法律以及禁止使用的過期魔道具，然後將這些東西修理好後拿來販賣。

至於她們從這邊進口的東西，是以魔族之國無法得到的魔物素材，或用那些素材制成的衣服、裝飾品為主。

新型魔道具，糧食之類的貿易品我們可沒碰過。

因為這些都涉及上層交涉的內容。

按萊拉小姐所說，目前她們並沒有忤逆魔族之國政府的白癡想法。

即便魔族之國真的再次被王族支配，那也是發生在很遙遠未來的事了。

我也覺得至少在現在的魔王大人在世期間不可能的。

所以，現目前她們就只是在積蓄實力而已。

再說，魔王大人的會社雇傭了大量失業的年輕魔族，其中很人以此為契機相識結婚並生下了孩子。

莫爾他們不久之前就來信告訴我他們的孩子出生了。

和萊拉小姐一樣還是獨身的新聞記者廬米小姐對魔王大人的會社的報導是這麼寫的。

『在這個少子高齡化社會中，很稀奇的可以期待出生率的會社』

具體內容也完全是一篇對積極雇傭年輕人，讓很多社員互相結婚生子的帶有善意的報導。

輿論對這篇報導的反應很不錯，除了一部分人外都沒有給出惡評。

畢竟，靠販賣古舊魔道具和技術給琳蓋亞大陸的人類賺錢的魔族如今已經變得很常見。

就算政府之間的交涉再沒進展，自發行動起來的人類和魔族也還是有很多嗎。

所有人都一邊想方設法的不觸及上面人物的逆鱗，一邊勉勉強強遵守底線的想出各種新生意來賺錢。

用權力阻止這類行為其實是相當困難的。

如果有武器或違法藥物走私的話確實會讓人頭疼，但即便不特意涉足那些領域也能賺到十足的利益。所以很少有人去做。

再說了，魔族的治安組織可是很優秀的，真幹那種事馬上就會被逮捕——廬米小姐曾經這麼告訴我。

「都已經拖了三年嗎。真虧他們幹得出來」

交涉無進展雖然對王國影響不大，但好像讓魔族之國變得很辛苦的樣子。

「民權黨派不上用場的批判聲越來越強了」

在民主國家裡，貿易交涉糾纏三年也毫無結果這種情況會遭到各方批判根本不難想像。

「已經有人說明年選舉時，民權黨肯定會從在野黨的位子上跌落了」

「這樣啊」

國權黨奪回政權的話，交涉也還是會繼續停滯嗎？

還是會比現在像樣些？

「這方面的情報我們雖然從不曾忘記定期確認，但現在什麼也做不了」

我已經不是交涉團的一員了。

現在，我只需要擔負起自己領地的責任就好。

「魔導水泵、魔導船、魔導飛行船、車輛、重型機械。雖然這些全都是從廢置區撿回來的東西，但只要稍微修理一下就能好好運作」

「我們全都會買下哦。陛下那邊有什麼想要的？」

「鸚鵡羽毛的需求特別大。現在用鸚鵡羽毛製作的羽毛被和羽毛枕非常流行……」

只要有羽毛，就能委託魔族之國的專業廠商製造這些東西。

所以萊拉小姐拜託我們盡可能多的把鸚鵡羽毛賣給她們。

「庫存有很多哦，所以你們希望的話我覺得可以再增加出貨量」

「因為討伐了非常多呢」

「雖然不是我動手的就是了」

吵著總有一天要打倒我的宋義智和之前總把要生下我的孩子掛在嘴上的DQN三人娘，率領著其他原本是士兵或武將的冒險者去狩獵了大量的魔物素材。

這其中鸚鵡羽毛賣的尤其好，甚至讓他們有錢在島上蓋起豪宅住了進去。

另外，DQN三人娘現在好像也懷孕了。

宋義智，居然不經意的獲得了成功。

因為已經這麼成功，所以他似乎不打算再回去領民很少的宋島的樣子。

「羽毛不管有多少我們都會收購的。羽絨被和羽絨枕總是處於缺貨狀態也讓我們很困擾的啊」

領地的開發和與魔族的交易都很順利，鮑麥斯特邊境伯領的開發一直處於高速狀態。

魔王大人也被魔族社會承認為女企業家了。

雖然上層的交涉毫無進展，但這三年我們可真是過得波瀾壯闊啊。

「啊，是魔王大人！」

「好久不見，魔王大人」

「露露和藤子嗎。寒假之後就沒見過面了吶」

商談結束後，魔王大人和人在我家的露露與藤子見了面。

小孩子組已經跨越種族差異成了相當要好的朋友。

現在魔王大人一有長假，就會為了商談和激勵留在鮑麥斯特邊境伯領工作的魔族社員來這邊視察兼遊玩。

『這也是朕為了成為真之魔王而對他國進行的視察兼遠征嘛』

說遠征魔王大人當然不可能佔領鮑麥斯特邊境伯領，不過作為攻佔領地的替代她一直在努力拓寬自家會社的販賣渠道，所以說是遠征（商業的）也未嘗不可。

也就是不動用武力的和平遠征。

「魔王大人，有買土特產嗎？」

「當然買了，露露。看吧，這是高級點心店『ESPOIR（譯注，現實也存在的蛋糕店）』的特級鮮奶油泡芙哦」

「好像很好吃呢」

「製作這類點心魔族壓倒性的優秀啊。雖然先前鮑邁斯堡和王都的點心水平提升到可以和魔族一較的程度，可魔族居然又變得更強了」

作為我的妻子候補留在我就的露露和藤子，這三年來對食物的口味變得相當高。

連這樣的她們看到魔王大人帶來的奶油泡芙後都兩眼放光。

這類高級消費品的品質，魔族那邊壓倒性的優秀。

問題就在於魔族中能消費的起這些東西的階層正在不斷變少這點。

也就是說魔族社會的貧富差距正在越來越大嗎。

琳蓋亞大陸上這方面也是差距的相當厲害。

「的確，這個看起來很美味」

「沒錯吧？鮑麥斯特邊境伯。這個雖然因為太貴平時不會買，但在這種時候就特別適合當做送給特定客戶的特定禮物。還能節省交際費接待費呢」

「計算的很細致啊」

「畢竟我們計劃的進展比預想的快了很多啊。所以可能會有人用魔王大人不正當挪用經費的醜聞來拖我們的後腿」

「明明現在連政治家都還不是？」

如果是政治家的話，賄賂、公款私用等行為的確會造成問題，可現在魔王大人並不是公僕。

我覺得並沒有小心到這種程度的必要吧……。

「即便朕現在還不是公僕，但也是將來有可能當上的立場。所以不能大意，必須從現在就開始小心」

明明還只是女子初中生的年紀，魔王大人卻相當努力。

魔王大人想當公僕？

將來還要打算走上政治家之道？

要是這樣的話，我倒是能理解她為什麼這麼小心謹慎的了。

「不過，首先得完成漫長的學生期。而且還有生下下一任王的大工作吶」

女孩子到了初中生的年紀都會對戀愛開始有興趣，不過她會把產子劃分到工作部分還真是厲害。

魔王大人大概今後也會喜歡上誰，然後談一場普通的戀愛吧。

「同級生中沒有陛下中意的男孩子嗎？」

「大家都太幼稚了」

在為了取回作為魔王的力量，完成學業至於總是和大人們一起經營會社的魔王大人看來，同小的同級生完全就是一群小孩。

看來魔王大人喜歡上比她年長男性的可能性很高啊。

「好了，都作為土產帶過來了不一起來吃嗎。朕買了很多，大家都不用客氣哦」

魔王大人吧奶油泡芙分給每個人，並宣稱要在鮑麥斯特邊境伯領暫住一段時間。

正巧就在這個時候，一個不得了的陰謀在背地裏成形了。

（以下是普拉特伯爵視角）

「所以我不是說過嗎！鮑麥斯特邊境伯才是一切的元兇！」

「那個男人，居然敢偷偷和魔族進行交易！」

「就是！我們魔道具的銷售額最近完全沒有增加！這也全都是鮑麥斯特邊境伯的錯！」

這裡的所有人都滿嘴不滿，其中大部分人的不滿對象都是鮑麥斯特邊境伯。

我就是這麼誘導他們的所以當然會變成這個樣子。

從長期擔任領頭人的會長死後，魔道具工會就一直這麼混亂。

他們非常憎恨從魔族那裡大量購入魔道具的鮑麥斯特邊境伯。

畢竟人類只會看對自己合適的事實。

的確，最早引進魔族制魔道具的是鮑麥斯特邊境伯沒錯，但現在只要多少有點眼光的貴族或商人都會這麼做吧。

既然現在政府之間關於交易的交涉還是沒有進展，那走私貿易當然就會普及化了。

就連王國和帝國政府自己，也都創建了假商會參與其中呢。

不過所有人都很有默契的禁止交易魔道具工會能製造的魔道具、武器、奴隸這類貿易品。

曾有些貧窮的地方貴族和不良魔族聯手做這方面的交易，但那些人大部分很快就東窗事發被改易了。

王國和帝國政府大概也為能清理一寫低能貴族、增加直轄領地而高呼萬歲吧？

雖然那種飛地一樣的貧窮領地，都出於統治也只會增加王國的負擔這個原因以賞賜的名義下賜給其他貴族就是了。

哦呀話題有點扯遠了吶。

魔道具工會前會長的葬禮結束後，經歷了好一番爭鬥現在的會長才終於上了臺。

這是個原本就掛著理事名頭的人物，管理組織的能力相當優秀。

然而，他自身所屬的派系規模卻不大。

他是對多數派做了很多工作，和其他派系做了諸多妥協後才就任會長的，所以沒什麼實力。

本人雖是魔道具工匠出身手藝卻不怎麼樣，很早就已經從實操工匠中抽身隱退了。

也因為這個原因，他完全拿現在不斷流入王國的魔族制魔道具沒辦法。

當然了，魔道具工會也有購入魔族制的魔道具進行分解和解析。

帝國魔道具工會也和北方技術國瑞穗的魔道具工匠們結成了同盟一起做著同樣的事。

王國魔道具工會和那邊唯一的區別，就在於完全沒有拿出任何成果這點吧？

畢竟他們過去還很闊氣的從鮑麥斯特邊境伯那裡購入了很多從遺跡中發掘出土的魔道具進行解析，結果也沒拿出過什麼成果。

在我看來，只會覺得這樣的魔道具工會的技術部門說不定有什麼致命的缺陷也不一定，不過這種話即便和那些人說也是徒勞吧。

『人窮志就短』，這句話說得還真好。

魔道具工會因技術進步停滯和市場被奪的危機感跑去妨礙政府間交易交涉的結果，就是惹怒了王國政府。

不過王國政府也無法直接處罰魔道具工會，畢竟他們的資金實力和政治影響力實在大的過頭，而且擊潰後又找不出替代的後繼組織。

所以，王太子殿下才組建了假商會去做私人貿易。

陛下自己不動由王太子殿下出面也是這一招的厲害之處。

不過，王國也不希望眼睜睜看著魔道具市場被魔族奪走。

現在王國的魔道具製造技術遠遠不如魔族，生產力也相當劣於對方。

承認這點，對拿來交易的魔道具的種類和數量加以限制，也把收取有關關稅納入視野中，以此來守護本國的魔道具產業並促進其成長。

王國政府明明為了達成這一切拚命努力，可關鍵的魔道具工會就怎麼也不肯放棄阻止魔族制魔道具進口的頑固態度。

現在已經有大量魔族制魔道具通過私人貿易流入了王國。

這一行為的領軍人物就是鮑麥斯特邊境伯。所以新會長和其追隨者都非常憎恨他。

明明迄今為止工會從鮑麥斯特邊境伯那裡購入了很多對方辛苦發掘出土的魔道具來著，這些人還真是會恩將仇報。

而且就是因為那個鮑麥斯特邊境伯率先在購入魔族制魔道具時對種類加以限制的，王國、帝國、其他貴族也模仿了他的做法。

所以，魔道具工會的銷售額其實並沒有下降。

說到底，人家進口的不全都是你們做不出來的東西嗎。

明明是這樣，這些人還是恨上了鮑麥斯特邊境伯，果然讓年過八十的偏執狂老頭擔任新會長是需要付出代價的。

前會長死時只會想到【這下輪到自己風光了】的這個人，完全就是個沒用的老不死禍害。。

不過，就因為他是愚者所以才好利用。

聚集在他身邊的人也是類似的貨色。

例如沒有享受到鮑麥斯特邊境伯躍進式發展恩惠，就因此記恨他的貴族。

也有在與魔族的交易中上當受騙後把過錯推到鮑麥斯特邊境伯頭上的人

我完全無法理解他們為什麼會產生這種想法，難道他們憎恨與魔族交易而產生的利益麼？不對，應該是不願意承認自己的無能吧。

甚至有人把和魔族交易的鮑麥斯特邊境伯視為賣國奴。

這些人雖然低劣的讓我看著就不舒服，但他們也有容易利用這個優點。

而且，反正我和他們也是同類。

我也很憎恨僅僅是把我的兒子塞進魔族之國的監獄，就在王國裡備受好評的鮑麥斯特邊境伯。

的確，我兒子在那件事中是有錯。

但即便如此，也不會有父親在兒子遭到那種對待後還無動於衷的吧。

而且他還給我到處散布我兒子是為了拯救其他船員才自己主動卻蹲監獄的傳聞！

那可是我兒子！

他才不會那麼為他人著想！

這種事，身為他父親的我是最清楚的！

所以我一眼就看破這是鮑麥斯特邊境伯為了姑息周圍的評價而特意用的手段。

現在，我制定了暗殺鮑麥斯特邊境伯的計劃。

這麼幹風險很大，而且不會有任何好處。

歷史悠久的普拉特伯爵家因此改易的危險也很高。

但是，我可是有感情的生物。

所以即便要冒家族被擊潰的危險，我也要處於感情沖動向鮑麥斯特邊境伯復仇。

再說了，我也有好好準備不會遭到處罰則策略。

讓聚集在這裡的愚者們充當替罪羊就行了。

暗殺所需的資金也是魔道具工會和憎恨鮑麥斯特邊境伯的貴族們出的。

連今天的秘密聚會我也沒有出場。

現在所缺的就是暗殺的手段和實行的棋子，這些我也早有所準備。

鮑麥斯特邊境伯是很厲害的魔法使。

即便找其他魔法使暗殺他也只會被反殺，而且還會讓人在幕後的我暴露。

帝國的魔法使也不能用。

因為鮑麥斯特邊境伯和帝國皇帝的關係也很好。

那麼，現在就只剩下一個選擇了。

「說起來，委託魔族暗殺鮑麥斯特邊境伯的門路，普拉特伯爵你有嗎？」

「有」

雖然我和鮑麥斯特邊境伯是路人皆知的對頭，但我姑且也是空軍派系的重鎮。

所以我擁有前往一直進行交涉的提拉哈雷斯群島與魔族見面的權利。

實際上我已經去過很多次了，也結識了些熟識的魔族政治家。

那群人是叫民權黨吧。

這群只靠嘴上功夫獲得政權的人當中無能之輩很多。

因為沒有成果而陷入焦躁的人很多，也有不少打著鬼主意的政治家。

這其中甚至有成為政治家前是做可疑的反政府活動的傢伙。

那麼，讓這種人幫我做中介就可以了。

尋找魔族暗殺者的中介。

如果事情真的暴露，兇手也是來自魔族之國的外國人。

王國應該也無法太強硬的追究。

「有關交涉就交給我吧」

「看你的了。普拉特伯爵」

按新魔道具工會會長的說法，既然要雇傭厲害的暗殺者那當然要提供重金。

這這麼多錢明明應該拿去增加研究經費吧，不過我終究是外人。

我一邊慶幸自己不必出錢就能成事，一邊急匆匆趕往提拉哈雷斯群島。

「交涉今天也沒有進展吶。嘛，那種事怎樣都好了」

我在迪拉哈雷斯群島當地和一名魔族議員會了面。

表面的名義是互相交換意見。

三個國家的政治家彼此像這樣的單獨會面的情況如今已經很普遍。

但與此同時，我也對這群被稱為民權黨的魔族政治家們的資質產生了很大疑問。

這些人雖然嘴上說的好聽，卻缺乏處理實際政務的能力……我總覺得這群人是為了能在稱為報紙的在庶民中傳達情報的公告出風頭，才會同意和人類單獨會面的。

我這個猜測似乎是正確的，現在為了上報紙而跑來和大貴族單獨會面的魔族政治家有增加的趨勢。

連那些原本不能加入交涉團的，因為魔族之國遲遲拿不出成功而對下次選舉當選產生了危機感的傢伙，也僅僅為了和大貴族會面不斷跑到這座島上來。

然後他們命令來取材的新聞記者好好把自己的誠意寫進報道中。

以此來向魔族的庶民們宣傳自己是在多麼努力的進行交涉。

當然了，這群人當中也有優秀的政治家。

然而糟糕的政治家更多，所以那些優秀的人完全被埋沒了。

一起同行的官僚們也因為上司沒用而完全陷入了束手無策狀態。

連我都清楚如果這些有實務能力的人幫手的話情況無疑會改善很多，但卻似乎不能採取這種做法。

率領王國交涉團的尤巴夏外務卿因為在新聞報道中非常惹眼，所以不斷接到魔族當權者出於宣傳自己目的而做的單獨交涉申請，搞得他非常疲憊。

帝國的交涉責任人似乎也是同樣情況。

『他們想要的，就只是和王國交涉團首席的我進行過交涉的這個事實啊。因為明年就要進行選舉了，所以魔族的當權者都瞄準了現在這個機會宣傳自己』

明明三年過去了交涉還是沒有進展，可尤巴夏外務卿的黑眼圈卻越來越明顯。

而且他原本就不是什麼身心很結實的人物。

肉體先不說，精神上他大概已經瀕臨極限了吧。

在這樣的背景下，找到另一種政治家就很簡單了。

也就是既沒有記入交涉團，對國家開放等長期問題也莫不關係的政治家。

這些人雖然靠運氣好當了一回議員，卻沒有下次競選也能當選的想法。

因為他們比那些拚命宣傳自己的議員們更清楚的看明白了現實。

這些人只會趁著擔任議員的機會，為了幫助下一段人生斂財拚命打響自己名字。

就在這群人當中尋找符合我條件的候補吧。

略微收集了情報後，我就發現了個完全符合自己條件的政治家。

話說是叫民主主義嗎。

這些魔族政治家因為自己的國家實踐了那個東西而感覺自己高人一等，但打了三年交道後我已經看穿了他們的真面目。

『政治系統無所謂優劣，會輕易左右最後結果的就只是運作系統人物的資質而已』

只有這點，我贊成鮑麥斯特邊境伯的意見。

所以我剛打開這處避人耳目的和我看上的議員會面場所的房門，就聽到了最開始那句相當沒幹勁的話。

反正就算這傢伙努力，也只會反過來讓狀況惡化而已。

所以，沒幹勁的態度反而比較好嗎。

「其實，我有件想秘密委託熟識的您解決的事情」

「哦？說來聽聽」

我已經獲得背後支持這名議員的組織是個不避諱動用暴力手段的市民團體這個情報了。

而且那些人有時還會染指牽扯到黑道的犯罪行為。

通常來說政治家都會隱瞞和這樣的團體有往來的關係，可這傢伙居然說自己是堂堂正正靠著那種組織的支持當選的，民主主義這玩意兒還真厲害啊。

雖然，現在先把話題集中到工作上吧。

「聽說，您手下有能包辦各種行動的手下？」

「有啊，想讓他們和誰玩玩嗎？」

議員沒幹勁的表情變了。

所謂玩玩，是他們的一種隱喻。

似乎是暗指遊玩對象就此死定的意思。

這群人最開始也是燃燒著使命感投身反政府活動的，但不管他怎麼拚命活動也看不到未來。

期間，他們當中出現了染指牽扯黑道的犯罪行為的人。

這些人逃過了政府的監視一直在鍛鍊自己。

魔族所有人都是優秀的魔法使，即便徒手也擁有莫大的戰鬥力，所以偶爾也會有人去殺人。

雖然如今的魔族大多很老實，但也不是說會犯下那種罪的人數就變成零了。

「那麼，他們要和誰玩玩呢？」

「鮑麥斯特邊境伯」

「……可以。不過遊玩資金可有點貴哦」

「真的可以嗎？」

雖然魔族議員猶豫了一下，但還是接了這單委託。

是他心裡有能殺掉鮑麥斯特邊境伯的魔族的人選了吧。

所謂遊玩資金，應該就是這群人通用的暗殺必須費用的隱喻了。

「我會付錢，用什麼方法支付？」

「金塊和寶石都可以，選你喜歡的就好」

畢竟現在關於貨幣兌換的規則還沒成形。

私人貿易也是這樣，用貴金屬或寶石進行以物易物的交易。

收取這種非公開的報酬，是為了免除稅金和保密吧。

「非常好，我會盡快給你具體指示」

因為又有個賺錢的機會，議員的嘴角翹了起來。

這傢伙真是個垃圾政治家。

魔族的技術雖然有很大進步，但看來在人性上卻沒什麼長進的樣子。

不過我也沒資格說別人。

「不過還真是不可思議啊」

「你指什麼？」

「沒想到你這麼乾脆就接了這次的遊戲」

鮑麥斯特邊境伯雖然只在私人貿易這種灰色地帶活躍，但到兩國的正式交易交涉有突破為止，他都會是世人眼中代表了兩國間貿易事業的人物。

我聽說被雇傭的魔族之國年輕人也因為他增多了，對這麼一個有功之人的暗殺委託真虧這個議員敢接啊。

雖然由提出委託的我這麼想有點怪。

「我們魔族也不是鐵板一塊的」

「你是指現在的政府吧？」

「的確政府也是，民權黨本來就是聚集了三教九流的人物後形成的哦」

成立民權黨的理由，就只是為了打破國權黨的獨裁政治。

結果，從極右到極左的勢力都加入到了民權黨當中。

「很多人都覺得民權黨的政治傾向只是稍微偏左而已。但其實並非如此，這就只是個為了打倒國權黨才聚集起來的政黨」

「真虧你們這樣還能運營政權啊」

「不能哦。整個組織已經確定明年肯定要四分五裂了」

所以，這個議員打算靠現在的地位最後大撈一筆嗎。

這樣的民權黨議員，似乎比我想像的還多。

「另外作為黨內主流的左派人物心裡，也很討厭鮑麥斯特邊境伯」

「因為魔王嗎？」

「沒錯，說真是讓一個麻煩的東西復活了」

復活？

魔王連公僕都還不是吧。

我覺得那女孩就只是靠私人貿易成功建立了一家大商會而已。

「現在那個魔王在民眾中人氣很高。正好和民權黨政權的支持率持續下降形成了對比呢」

魔王的會社也不是憑空出現的，那裡雇傭了很多魔族年輕人

而且待遇還不錯。

魔王的會社不僅讓很多同社社員結了婚，還幫他們育兒。

「民眾也沒想到啊。民權黨的失業對策，其實就是浪費稅金建造類似這房子的完就拆的設施，來短期雇傭年輕人們，一次暫時將失業率數字壓低罷了。相對，魔王卻會好好的正經雇傭年輕人們。雖然不受歡迎的政府就特別看魔王不爽。還有些笨蛋吵吵著這麼下去封建制度就要復活了什麼的呢。而且，那還都是些在政府中樞任職的傢伙們，民眾自身真有那樣想法的人反而幾乎沒有。說到底，事到如今了魔王到底怎麼才能即位啊？」

雖然是剛察覺的，但這個議員似乎是個表面裝成傻瓜其實頭腦很靈活的人物。

年輕時為了革命奔走的他，終於成為夢想中的擔當政權的一分子後卻發現現狀什麼也沒有改變甚至是惡化了，所以絕望了吧。

他已經再也不想碰政治了，所以現在才以錢為優先嗎。

這麼一想的話，這傢伙也是個受害者？

「總之就是這麼回事，因為魔王飛黃騰達了鮑麥斯特邊境伯也就被人恨上了」

我聽說魔族出口的魔道具原本只是垃圾，可現在卻有魔族吵吵著這些東西會泄漏魔族的技術。

在這些人看來，魔王和其手下都是賣國奴，鮑麥斯特邊境伯則是為虎作倀的協助者嗎。

「這可是單大買賣，我得準備些身手高明的人陪鮑麥斯特邊境伯玩，而且還得從和我們組織立場正相反的人群裡選人」

也就是說，他要從世間看來和國權黨畢竟親近的偏右組織中找人吧。

這麼一來，民眾就會把暗殺鮑麥斯特邊境伯當成是國權黨下的手了吧。

政治家這種東西，不管道路哪個國家都是一路貨色。

我也是其中的一員嗎。

「原來如此，我知道了」

敲定好所需費用金額後，我把代替報酬的金塊和寶石交給議員讓他轉交給暗殺者，然後就返回了本國。

這樣骰子就扔出去了。

接下來，就是向神期待能讓死亡順利降到鮑麥斯特邊境伯頭上。

鮑麥斯特邊境伯，你已經沒有什麼接下來了！

接受讓我兒子被關進監獄的報應吧！

「我是『世界征服同盟』的奧斯托·海因茲」

我也是極左組織出身所以沒資格多說別人，但這傢伙也相當那個啊。

真虧他能滿不在乎的為組織取了這麼個丟人的名字

但反過來說，正因為他是這樣的笨蛋所以才適合當棄子。

這傢伙和一部分國權黨議員有很深的關聯。

如果他的身份在暗殺鮑邁斯特邊境伯後曝光……不如說能曝光的話最好……那就能拿到一個打擊國權黨的好材料了。

「我的委託只有一個。就是要鮑邁斯特邊境伯死」

「他的家人和家臣呢？」

「那傢伙本人死掉就行。其他人要一起收拾還是放生是你的自由。我只要鮑邁斯特邊境伯本人的命」

我可是收了重金接下這個委託的呢。

所以不好好實現委托人的要求可不行。

不管是魔族還是人類，錢都是很重要的。

「僅靠我一人無法成事」

「你用什麼方法我不管。只要能讓鮑邁斯特邊境伯死掉就行」

「那麼，就當做我和同伴們一起接下這委託吧。我們會討伐從我國盜走技術和財富的鮑邁斯特邊境伯！」

感覺又看到了什麼很不得了的東西。

能蠢成這樣，倒是不用懷疑他們和民權黨有關了。

這傢伙似乎在運營著名為『世界征服同盟』的可疑政治團體，旗下人員大約有十名左右。

包括被吸引的人在內他們能動用的人員也就是數十人的程度。

因為是這種微不足道的政治團體，所以如果萬一能成功暗殺他們憎恨的鮑麥斯特邊境伯，他們的名號就可以打響。

如果他們拿這次的報酬做今後活動資金，那這次的委託對於他們而言也是算救急吧。

「我們會盡快裝成協助走私貿易的商會接近鮑麥斯特邊境伯」

「用什麼辦法都交給你決定。這是定金」

我將金塊交到奧斯托瘦弱的手上後，黃金的重量讓他吃了一驚。

畢竟是重到無職的貧窮年輕人無緣接觸的金塊。

「那麼。就看你的了」

「交給我吧」

後面的事交給奧斯托就好。

對我來說，這次的委託成功不成功其實都無所謂

這次可真是賺到了，我露出帶著這樣意思的開心笑容，是在奧斯托離開之後發生的事。

「噢噢，真壯觀啊。要是魔族也能生這麼多孩子朕就太開心了」

享受完點心時間後的魔王大人來嬰兒們就寢的房間參觀時，被嬰兒過多的數量驚到了。

目前，我自己的孩子數量就已經超過了二十名。

艾爾和羅德里希的孩子也在這裡，其他家臣的孩子們則在隔壁更大的房間裡睡覺。

這裡已經完全是個育嬰院了，家臣的妻子們正很有效率的照看著他們。

「朕將來也必須生下作為下任魔王的孩子才行。也必須有支持王的王族。所以朕想生三個孩子」

這位魔王大人雖然外表看上去只有女中學生的年紀，說起話來就像個正在為結婚活動的女性。

「陛下喜歡什麼樣的男性？」

「這個嘛。下任王不能是愚王，所以溫柔又優秀的男性比較好。另外外貌對於王很重要，因此朕希望丈夫的臉能盡可能美型些」

「陛下，世間管您這種標準叫做要求過高。而要求過高是會成為錯過婚期的重要原因的……」

明明漂亮又能幹卻總結不了婚的萊拉小姐特意叮囑了一下魔王大人。

她有著最初莫爾他們向她搭訕時明明心裡感覺並不差可態度卻太冷淡，導致那三人不久後便逃走的過去。

萊拉小姐似乎認為最開始時態度冷淡些反而更能引發男性執著心，結果沒想到莫爾他們居然會那麼乾脆就放棄追求她把目標轉向了其他女性。

結果，萊拉小姐直到今天還是獨身。

結婚的預定……也不知道她有沒有？

畢竟她作為社長很忙啊。

「就聽從萊拉的忠告吧。朕只要個是溫柔的男性就好」

溫柔的男性嗎……。

這種條件有點曖昧誒……。

「陛下，只是夠溫柔的男性可不適合做丈夫哦」

「是嗎？」

「是的。就算再溫柔，沒出息的男性也不好。雖然我覺得不必太拘泥於男性的財力，但婚後生活是很漫長的。有很多夫婦都因為沒錢導致關係和生活產生了摩擦」

「原來如此。萊拉知道的很詳細吶」

「明明沒結過婚呢「噓———！」」

我慌忙堵住露易絲的嘴巴。

「（露易絲，就算是沒有結婚經驗的人也是可以幫人做結婚諮詢的哦）」

「（這樣啊，也就是拿世間的一般常識做參考指指點點的做法吧）」

有的人不是明明沒有打棒球的經驗，看比賽時卻能對選手的打法和監督什麼的講解的頭頭是道嗎？

和那個一樣啦。

「這種時候，鮑麥斯特邊境伯是人類真是太可惜了」

抱歉，我不需要繼續再增加妻子了。

「朕還不到能生孩子的年紀。所以沒必著焦急。話說回來，他們還真可愛不是嗎」

「萊拉小姐就差不多該著急了……「噓———！」」

我又堵住了露易絲的嘴

魔王大人則抱起露易絲的兒子奧拉菲逗他玩。

她是為了將來而想實際學習照顧嬰兒的方法麼。

「話說回來，關於西海岸建設中的海水淡化設備……」

萬幸的是，萊拉小姐無視了露易絲的發言。

她提到的海水淡化裝置，是在秋津洲島已經有數座小型版在運行的東西。

這種裝置可以將海水淡化為淡水後輸送到水不足的場所去。

是在魔族之國從很久之前就理所當然使用的東西。

魔族人口後減少後很弱這種裝置被扔在了廢棄地，魔王大人她們把這些裝置回收、修理後送到鮑麥斯特邊境伯領繼續使用。

如果換成現在琳蓋亞大陸魔道具工會製作的同類試作品，光是每天生產幾公升淡水就能讓製作者們狂喜不已了，所以這種裝置我們只能從魔族那裡購買

雖是被拋棄的古舊裝置，但狀態有問題的話很簡單就能修理好。

也就是說，哪怕這裝置的性能只有最新型同類產品的幾分之一導致在魔族之國被當做大型垃圾，拿到琳蓋亞大陸的缺水地域也會成為救世主一樣的存在

唯一的問題，就是如果不定期補充魔力就無法使用這點。

不過在有很多魔法使居住的鮑麥斯特邊境伯領就可以很從容的設置了。

鮑麥斯特邊境伯領的西方海岸因為都是懸崖峭壁還有讓船無法靠近的海流，所以給人種開發落後於別處的感覺。

而最致命的就是這一帶河川比較少這個瓶頸，所以即便我們往這裡增加人口也很容易陷入缺水的狀態

所以，我們要在這裡建造魔導飛行船的港口，將從海里提取海水淡化成的淡水送往各地，這個送水系統的構築也得到了魔王大人會社的協助。

「海水淡化裝置的成品預定什麼時候送到？」

「計劃會在一周到十日之內送達」

「這次有點慢啊」

「因為現在必須去更遠的廢棄地回收魔道具了呢。而且也有委託增加導致的人手不足理由在。雖然也雇了新人，但最開始得先做好相關教育才行。所以這次我們委託了其他業者運送裝置」

「嘿誒，是這樣啊」

「年輕人當中想和人類做生意的人現在增多了。其中有不少人希望靠接受我們這些行業先行者的委託來積累業內經驗」

就是說隨著交易增加，魔王大人會社的狀態也會更好嗎。

「運貨的是社員有十名左右，登錄會社名為『奧斯托運輸』的小會社。對於他們光是運這一次貨的報酬就是一大筆錢了。使用舊船這種地下手法他們也會用。雖是一群年輕人，但也擁有購買船只和創業的資金」

人類在經濟上侵略魔族之國。

也有可能反過來。

不過，感覺人類魔族雙方都不會把這種危險性說出來，只是為了尋求利益而摸索出各種形式的交易。

既然兩個種族已經邂逅，那麼就只能彼此打交道下去共同經歷各種實驗錯誤了嗎。

「就是說等那個『奧斯托運輸』的船抵達卸下貨物後就可以開始設置了嗎」

「是的。鮑麥斯特邊境伯要一起參觀設置作業嗎？」

「這主意不錯呢。露易絲要不要也一起來？」

「好啊。雖然對其他人不好意思，但我就把孩子交給別人照顧外出一下吧」

「這樣不也挺好？」

露易絲平時有好好照顧孩子們，其他人也不會不管她的孩子。

妻子們要是因為育兒患上神經衰弱的話我也會很困擾，所以也邀請了埃莉絲她們來。

「得去和大家說一聲了呢」

就這樣，我們決定去西部海岸附近視察，但卻被卷入了意想不到的事件中。

「哼哼哼，不僅靠預付的報酬買了這艘中古船，還幸運的接到了往鮑麥斯特邊境伯領運貨的工作。接下來，就只剩將鮑麥斯特邊境伯討伐掉了！」

我的計劃是完美的。

鮑麥斯特邊境伯一定想不到運魔道具來的送貨人是暗殺者吧。

話說回來，人類果然不可救藥。

居然敢做出用低價購買集合了我等魔族睿智的貴重魔道具這種行為！

「有嗎？這些基本都是大型垃圾吧？」

「約瑟夫，即便是大型垃圾這裡面也包含了魔族貴重的技術和睿智。這樣的物品流入人類那邊的話肯定會被對方模仿，最後導致未來以數取勝的人類魔道具壓倒魔族魔道具……」

為了這次能成功，我召集了所有世界征服同盟的同伴。

大家都是能和我的理想共鳴的優秀人物。

他們只是運氣不好才無職，但今後就不同了！

討伐掉鮑麥斯特邊境伯的話，我們世界征服同盟的名字一定會在國內廣為傳播。

到時肯定能聚集到更多贊同我們壯舉的同伴吧，這樣進入政界也不是夢想了。

暗殺完鮑麥斯特邊境伯後只要離開琳蓋亞大陸就不會被問罪。

能制裁我們暗殺鮑麥斯特邊境伯之罪的只有赫爾姆特王國，而那個國家的力量觸及不到我們國家

交涉三年還毫無進展的兩國之間，怎麼可能做得來引渡犯罪者的交涉。

這種時候，民權黨的無能反而幫了我們的忙。

反正是群會在下次選舉中消失的傢伙。

話雖如此，讓國權黨再次取回政權對國家也不好。

此時，就該輪到作為第三勢力的我等出人頭地了。

鮑麥斯特邊境伯，你就為了我們犧牲吧。

我雖然是這麼想的，但同志之一的約瑟夫卻還在囉嗦。

「可這些東西廢做棄處理的話需要成本，不當拋棄的話還會造成問題的不是嗎。而且很多人都說這種東西消失的話對環境也有好處」

「即便如此，那些人類，尤其是鮑麥斯特邊境伯可是用不合理的低價購買這些東西的！這完全是掠奪我等魔族財富的行為！」

「積壓的舊貨能一下清理乾淨，對企業業績好像也挺有幫助的樣子。因為賣掉舊魔道具後購入新品的人增加了嘛」

啊啊！

你怎麼這麼羅裡吧嗦的、

但是，我可不是魔王那樣的專制君主。

聽取統治意見的肚量我還是有的哦。

「企業？那種從我們這些年輕人這裡奪走工作的組織賺到錢對國家有什麼好處！不如說，那些人的生意順利只會讓庶民們陷入更深的貧困而已！所以我們才要接受討伐鮑麥斯特邊境伯的工作。連這艘船都是用那件工作的報酬買的。所以我們沒法拒絕這份工作吧！」

「這點我也明白……」

是嗎，原來你明白啊。

把話說清楚了就能明白是好事。

「我說，奧斯托」

「還有什麼事？雷歐」

「我們贏得了鮑麥斯特邊境伯嗎？」

「能贏！我的魔力和那個魔王相比也不輸呢」

雖然從沒聽說我的祖先裡有什麼王族出身的大貴族，但我的魔力非常多。

不過在我迄今為止的人生裡這些魔力一次都沒派上過用場

不僅是我。

我們這十人魔力量全都很多。

「我會盯著鮑麥斯特邊境伯戰鬥，也期待諸位能好好奮戰」

「聽說鮑麥斯特邊境伯周圍有很多魔法使」

「視察是不會所有人全來的！諸位，你們只需要讓其他護衛無法靠近鮑麥斯特邊境伯就行！就算戰鬥也不必非贏不可。只要幫我拖延時間到鮑麥斯特邊境伯被討伐為止就可以了！」

對，沒必要殺掉其他人。

只殺掉鮑麥斯特邊境伯然後迅速逃走就好。

「作戰我明白了，但還有個擔心的地方」

「什麼？凱歇爾」

在我們當中擔任參謀職責的凱歇爾，對我的作戰提出了一個擔憂。

這類問題點現在能趁現在找出來也好。

「聽說鮑麥斯特邊境伯的戰鬥經驗很豐富。相對的我等卻沒有戰鬥經驗」

「就是說啊。我等只擁有大量魔法訓練的經驗，戰鬥什麼的還沒經歷過。至少先去狩獵下魔物比較好吧？」

事到如今再說這些也晚了。

曾經有人提出過類似的建議，但大家都以討厭見血為由反對。

是覺得殺死不能吃的魔物這種無意義的行為不好吧。

「如果至少把肉帶走的話就不算白白殺死魔物了，但我們當中沒有具備肢解魔物經驗人。算了，這些問題到時再想法補救吧」

就算有再多戰鬥經驗，對方終究是人類。

和其他成員不同，我的魔力量可是比鮑麥斯特邊境伯還多的。

「總之只要能讓我和鮑麥斯特邊境伯一對一的戰鬥就可以。而且那是最糟的情況。我們預定會對毫無警戒的鮑麥斯特邊境伯發動奇襲，所以只要在交接貨物時找個空隙向鮑麥斯特邊境伯襲擊過去就好。即便是老練戰士的他也想不到我們會來這一招吧」

我的作戰計劃是完美的。

現在曖昧不清的魔族和人類的關係這下將立分高下。

魔族會壓倒人類，優先權將落入我等魔族手中。

我是很聰明的，才不會要什麼人類的領地。

現在這個時代，掌握住技術、生產力、流通、財富的人才是贏家。

我會成為將琳蓋亞大陸變為魔族的經濟殖民地，給予無職、低收入年輕人們希望的存在！

「首先是第一步，將鮑麥斯特邊境伯討伐吧！」

「「「「「「「「「「噢噢！」」」」」」」」」」

接下來，就只剩起事了。

「嗯……。奧斯托運輸的社長先生，這些就是要運的貨物了」

「明白。要盡快送到是吧」

「拜託請以安全優先，社長和會長是這麼說的哦。畢竟就算慢一點，考慮到兩國的距離也是沒辦法的事」

「這樣啊。那麼就得安全航行才行了」

我們自費購入的魔導飛行船載上從那個自稱魔王的邪惡殘忍專制君主子孫的會社那裡接收的貨物後，一路向著鮑麥斯特邊境伯裡飛去
