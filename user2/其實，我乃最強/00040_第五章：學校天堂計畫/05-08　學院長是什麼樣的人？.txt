也許就不用去上課了。
抱著這樣的期待，睡得很熟的我，第二天早晨來到提亞教授的研究室。

在學校過著家裏蹲生活的時候，作為據點的房間，得到了『這裡可以使用哦』的許可。但是，因為是成為儲物櫃的地方，所以需要掃除。
很麻煩，但只有這個辦法。最不需要的東西是向謎之時空送去的話，就會變乾淨哦。

房間在二樓拐角處。
開心地走過來看看。

「早上好！兄長。」

挽起手臂，把頭髮向後紮成一束，穿圍裙的妳啊，

「夏爾，在幹什麼呢？」

「我聽說從今天開始，這個房間將成為兄長的據點。所以我來打掃了。」

微笑的我的天使夏洛特。對了對了，昨晚說了那麼些話。行動迅速。
在那樣的她後面飛出的紅色的影子。

「呵呵，說到打掃的話，就是女僕了。說到女僕，就是這個芙蕾。就交給她收拾吧！」

沉浸在女僕工作的魔族小姐，在走廊裡抱著巨大的破爛，轟的一聲。到底是怎麼做的？

「啊，哈特先生。早上好。」

這次麗莎出現了。這邊也拿著大破爛，堆到走廊。所以那個……我來處理吧。
順帶一提，麗莎隱藏了角和尾巴，芙蕾的耳朵和尾巴卻這樣留著。在這裡不用擔心嗎？

好不容易妹妹來打掃，我也想作為兄長來幫忙。

「嗯～……？怎麼了，從早上開始就很吵啊。」

揉著眼皮走來的是身穿睡衣的迪亞教授。頭上戴著像聖誕老人帽一樣的睡帽。難得在床上睡著了。

「提亞利埃塔教授，早上好♪」

夏爾滿面笑容地打招呼時，提亞教授驚訝地說道。

「啊，那個……初次見面。妳是哪位呢？」

說起來和夏爾本人是初次見面啊。神秘的天才研究者威伊斯．奧爾用白面具掩蓋了原形。這個是暴露，不過，這裡好像合起了話。

但是，就是這樣。　
夏爾：「嗯？」略微歪頭。過了一會才恍然大悟。

「初次見面！是的，妳和我是初次見面。聽說是那樣。我是兄長大人的妹妹夏洛特．芬菲斯。」

「……謝謝您太客氣了。」

那樣一邊還，靠近我小聲說。

「那孩子沒事吧？雖然我很擔心。」

「請用豁達的眼光來看。如果順便有什麼事的話，請幫我善後。」

「那是你的職責吧？我很抱歉。」

悄悄地說著話而驚慌失措的夏爾無法置之不理，我開始幫忙。

「這個破爛可以扔掉嗎？」

「不行。雖然我知道已經毫無用處，但我相信總有一天會有用的。」

「沒打算要用吧？扔掉吧。」

「太可惜了！」

「太浪費空間了。」

排除不能斷舍離的廢柴教授的反對，我把廢物扔進神秘時空。

「收納魔法！？怎麼做的！？」

更煩人了。
麗莎輕輕地靠了過來。

「那個，哈特大人？可以給她看古代魔法嗎？」

這麼說來，我還沒說，我告訴過提亞教授我是濕婆呢。

「沒關係。這一帶都調整得很好了。」

「這樣啊。哈特大人這麼說的話，我就放心了。」

完全相信我的眼睛太耀眼了。我們可以另外談談，但總覺得時機不對。

不管怎麼說，四個人打掃的話打掃得快。一轉眼地收拾起來，地板和牆壁都閃閃發光。比宿舍的房間大一點。但是房間裡什麼都沒有。

從謎一樣的時空取出床、衣櫃等配置。非常興奮的提亞教授，勒緊了芙蕾。

據點的準備工作基本上都準備好了。以後，我的樂園和這個房間就用『任意門』連接。
那等提亞教授不在了再做吧。我不喜歡被人衝到樂園裡。

雖然中午還有點早，但是大家一起移動到寬敞的房間裡，貪婪地吃著芙蕾做的便當。

「話說回來，貝爾卡姆教授做得還好嗎？」

她應該帶著其他老師，和學院方面商量好讓我不用去上課。

說曹操，曹操到。
走廊發出大步走路的聲音。

「哈特．芬菲斯在嗎？」

貝爾卡姆教授，歡迎。

「在啊。嗯？有兩個不認識的人。而且那個紅髮……是魔族嗎？」

糟糕，糟糕。芙蕾的耳朵和尾巴就那樣。

「算了。」

行嗎？

「貝爾卡姆教授，您辛苦了。談判怎麼樣？」

「嗯，很順利。之後就看你了。」

「謝謝！」

這樣一來，探索遺跡什麼的，就被關進學校裡了。好極了。

「據說學院長會做詳細的說明。從現在開始。」

「誒！？」

驚訝的聲音不是我。提亞教授。

「等一下。奧拉醬，真的談判成功了嗎？」

「雖然說了『很順利』？我們的教師陣容的要求傳達了。學院長說大致表示理解了喲。她說最終的判斷是先聽哈特．芬菲斯本人的話再做決定。」

不上課的是我，聽她本人講話很麻煩，但我覺得這是理所當然的。然而——。

「那個，大概談判沒有失敗吧？」

提亞教授說了奇怪的話。

「……哈特．芬菲斯的話應該沒問題吧。」

貝爾卡姆教授為何移開視線？

「學院長給人一種很糟糕的感覺吧？」

只看過她在入學典禮上致詞，感覺像是個大姐姐（實際年齡相當大）。因為聽到一半睡著了，所以不太記得了。

「並不是性格彆扭。我反而不知道那麼正直的人。不，這裡有一個人比肩的孩子，但是系統完全相反。正因為如此，我和你的相性才是最糟糕的。」

直率的孩子是夏爾嗎？系統完全相反是什麼？

「你應該注意的只有一個。」

提亞教授將手指伸到我的鼻尖說道。

「扮演認真勤奮的學生！」

啊，好的。那不行。
我總算把話咽下去了――。