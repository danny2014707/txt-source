寶劍畫出一道紫光，割破了布魯達的頭皮，結果卻不得不停了下來，藏在布魯達衣袖裡的針稍稍刺進了我的胸口。
互相都動彈不得。只要再進一步，彼此的生命就都會消逝，在這樣的狹間，
“——雙方馬上放下武器。即使你們再怎麼愚蠢，也該知道這是最好的做法。”
空氣變得沉重，有質感的聲音在小巷裡響起。聽到那個聲音，感覺從自己的嘴中吐出的氣都變得沉重了。
耳熟的聲音，使背脊產生刺痛感的聲音，是曾經聽到過的鋼鐵公主薇斯塔莉努的聲音吧。

不妙，糟糕。

在這裡就被她注意到，實在是最糟糕的情況。如果變成那樣，我在貝爾菲因的一舉一動都會被她盯著。
放棄吧，在這裡放下武器，才是最明智的選擇。

這對布魯達來說也是一樣的，對於以貝爾菲因為據點的他來說，和鋼鐵公主作對是不會有好果子吃的。
“傭兵，還是流浪者？不要在這種地方浪費時間。”
那是不容置疑的語氣。當然，我們應該做的事情也不是對她的言語進行反抗，而是在這裡放下武器，發誓不會對她進行攻擊。

啊，對。現在重要的不是鋼鐵公主正確與否。現在重要的是，她是領主的獨生女，上流階級，而我們毋庸置疑是低劣的平民。
就是說，不管我們佔不佔理，都應該向她下跪。我們是被統治者，鋼之公主薇斯塔莉努，像她這樣的人無疑是統治者。

那麼說來，以前來貝爾菲因的時候好像也發生過一樣的事？那時她冷靜和蔑視的目光，我記得很清楚。
但是，沒辦法。就是這樣。階級本身就是力量，是無法顛覆的東西。最近完全忘記了這回事啊。因為，最近，明明階級有著天壤之別卻尊重我的人變多了啊。

與布魯達對視了一眼。就這樣，不眨眼，慢慢地，默默地放下武器。這是正確的，應採取的方式。
“可以吧。為了不讓貝爾菲因再發生無謂的爭執——流浪者就是這樣玷污貝爾菲因的。”
原來如此，連名字都不打算問，似乎也沒有在意樣貌。
不過正好，這樣粗枝大葉的對待，正合我意。不然，我的計劃就會全盤崩潰。如我所料，太棒了，沒有任何問題。就應該這樣。

明明這麼想。心臟卻快要融化般的灼燒著，指甲挖著手心的肉，脊椎燃燒起來，血液循環也加快了。
屈辱。啊，真是屈辱。現在我像被釘在了陰暗冰冷的海岸上一樣，身體被奪去了尊嚴，整個人都凍僵了，恥辱感從內心升起。

鋼鐵公主，薇斯塔莉努眼裡並沒有我們，我想她打心眼裡認為我們的爭鬥是無關緊要的，不會有任何的意義。無論我和布魯達賭上了多麼大的榮耀，無論我們是在多麼大的決心下揮舞著劍，都無所謂。

我不正是為了不再體會這種屈辱，才回來的嗎？難道不是為了不再讓任何人踐踏自己才在這裡的嗎？

明明如此，我還對這座城市的領主的女兒低下了頭。噢，我得感謝她。
我明白了，最終我還是什麼都沒有得到。打從心底感謝她。

鋼鐵公主薇斯塔莉努的魔獸馬，背向後街，沒必要呆在這種骯髒的地方，甚至都沒必要看著這種地方。馬蹄聲如此宣告著。
“——在馬上看起來很了不起地鄙視著呢。誒？”
兩道聲音重疊在一起，言辭的細節略有不同。但是，聲音的語調和所含的熱量完全相同。
我和身旁站立起來的布魯達同時發出聲音。雖然音量並不大，但那聲音清晰地傳到了鋼鐵公主的背上。

那個聲音，震撼著鋼鐵公主的背脊。那張臉，一瞬間轉了過來。
然後，平時非常淡漠的面部浮現出難以想像的笑容，說道：
“——我記住你們的面孔了，不要再奢望能在這裡過上安穩的生活了。”
既不對我們的話語做出回應，也不反擊，只是無視了我們的言語。鋼鐵公主騎在馬上搖晃著，悠閑地離開了小巷。

啊啊，真是被瞧不起了。

◇◆◇◆

“我沒自信說自己很聰明，但你一定是個不折不扣的傻瓜。”
我接受邀請，進入了布魯達作為據點的紅燈區的房間。

這傢伙，以前也以同樣的地方為據點，是喜歡妓院嗎？但是，我從沒見過這傢伙買春。不知不覺地伸出手肘，眯起眼睛。

剛才還互相以對方的生命為目標，現在卻這樣在同一個房間裡呼吸著。真是奇妙，這就是所謂的同病相怜嗎？畢竟現在是被這個城市的支配者——鋼鐵公主盯上了的狀態。只是普通地在酒館捲著煙袋，說不定就會被鋼鐵公主的仰慕者們盯上性命。

“啊，啊。當然知道。從出生到現在，我從沒覺得自己聰明。”
不，不管怎麼想，都是個大傻瓜。

不管怎麼做，都能走上最糟糕的那條道路。我為什麼要張嘴啊？那沒有意義，並不妥當，也不是明智的判斷。就好像親自踏進泥沼一樣。我真是個傻瓜，我。
“什麼，確實不是在誇獎你。但我不討厭這樣，也不是在說你壞話。”
這似乎是布魯達的坦言，是非常坦率的口吻。
布魯達微笑著補充到：至少不是用那些人盡皆知的小把戲。
真是微妙的信任。明知無能而信任對方，這應該挺稀奇的吧。

房間裡沒有椅子，像家具的東西只有桌子和窄窄的床。因此，無可奈何地坐在床上，布魯達抬起臉頰，歪了歪帽子，然後把便宜的朗姆酒倒進陶器裡。

就這樣，他並不開口。只是靜靜地等待著什麼。這樣的動作，是他尋求對方說話時的姿勢。
他好像覺得不好好聽對方說話的話會不好意思。

一邊回憶著那令人懷念的時光，一邊露出牙齒說出話語。從周圍傳來為攬生意而竭盡全力的女性的艷麗的聲音。
“布魯達，如果你打算繼續留在這座城市，我願意雇你當傭兵。”
倒入嘴裡的朗姆酒灼燒著喉嚨。哦，真喜歡質量不好的酒啊，這傢伙。
“內容是——貝爾菲因的兩輪，取下其中一環。就這些”
布魯達纖細的眼睛突然睜大。你在說什麼的疑惑和該如何回答的猶豫，在瞳孔中搖曳著。

但是，我已經知道答案了。雖然多少有些動搖，但他的內心應該已經決定好了。

總而言之，曾經的朋友布魯達，他留在貝爾菲因的理由，和我的意圖完全一致。