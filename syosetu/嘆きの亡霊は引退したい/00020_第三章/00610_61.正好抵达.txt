氏族的會長所處於的立場極為複雜。

氏族這一組織的體系原本就是由多支隊伍構成，因此容易使得事情復雜化。
對於我來說，除了《嘆息之亡靈》以外的氏族成員，只是關係比起其他陌生隊伍還要親近，但絕不是自己人。

雖然都統稱為氏族，不過其目的與組織體系是五花八門。

既有會長具備絕對性指揮權的氏族，也有掛著氏族的名號，各個隊伍單獨行動的氏族。
還有硬是拉攏低級隊伍往上面爬的惡劣氏族，更有以隊伍裡具有貴族門路來吸引其他隊伍加入，從而形成一個派系的氏族。

《起始之足跡》是基於彼此交換情報以及探索上的支援而建立，十分平凡的一個氏族。

我創建氏族的意圖並非是為了財富或者權利，所以比起其他氏族，估計會安逸許多。

我定下的規矩就只有三條。

大家友好相處。
不要給普通居民帶來麻煩。
以及民主主義。

雖然還有伊娃她們定下的瑣碎規定，不過按大的規定來說就只有這些。

像《足跡》這樣由幾支相同層次的隊伍組成的氏族，本來是難以選出位居頂端的氏族會長。
獵人重視名次，如果是認定等級差距過大，估計還能接受事實，否則無論是誰都應該希望成為會長的是自己的隊長。

而《足跡》是通過會長幾乎不具備任何權限的方式，以此解決這件難事。

《起始之足跡》的會長由多數票決定，並且對於自家的隊伍沒有任何的指揮權。

我家氏族的成員之所以會聽從我的請求，直白的說，這是他們的一片好心。

然而這樣的事實並不能讓我的青梅竹馬給他們留點情面。
絲特莉與莉茲或路克不同，雖然不會用暴力解決事情，不過比較起來其實也沒什麼區別。

「沒問題嗎⋯⋯他們不會退出氏族吧⋯⋯」

「我覺得是沒問題的，克萊伊。那些人遲早會知道你的一片苦心」

氏族的會長室。我坐在固定的位置上，回想著這次休息廳發生的騷動而發出牢騷，而本次的主犯垂下眼簾說道。

為什麼會變成像是我的錯一樣⋯⋯⋯

我只是稍微拜託一下別人而已⋯⋯這種做法也太卑鄙了。
在中途時，那氣氛就已經不是可以阻止得了的。她具有煽動別人的才能。
在言語上是很難說過絲特莉的。從以前開始我就總是說不過她。

「嗯嗯，也是呢？　⋯⋯不過，我還是希望事情可以更加和諧地結束啊」

和諧地、和平地。退一步講，如果被人趕下氏族會長的寶座那還算好的，再這樣下去怕不是會被人捅死。
聽完我所說，絲特莉臉上浮現出陰霾的神色。

「說的也是呢。⋯⋯就跟克萊伊說的一樣。那個⋯⋯他們比起預料中的⋯⋯還要軟弱。同樣是氏族的成員⋯⋯我覺得這樣真是羞恥。更何況⋯⋯⋯⋯他們還把自己的弱小都怪在我的藥水上！！」

緹諾跟在我們後面若無其事地踏入禁止出入的氏族會長室，她不知為何睜大眼睛，像是感到動搖一般仰望著我。

我可沒說⋯⋯我可沒說，想都沒想過喔。
軟弱之類的，這字眼可不是適合用在以破竹之勢發展的《起始之足跡》身上。

「歸根究底！如果那樣都要說出喪氣話──那露西亞又要怎麼說！！毎天都要不分場合地被人用笑容強制叫去充填寶具，一想到她──我就覺得可怜──嗚嗚⋯⋯」

絲特莉以更加顫抖的聲音大喊。她緊咬嘴唇，眼裡還啜著淚水。
簡直就像是悲劇的女主角一樣，這未免也演過頭了。

被說到痛處，我決定不說話了。
雖然我實在是沒有印象，不過要是這樣說一定會是火上澆油吧。

緹諾的眼神更加冰冷，就像是望著沒有良心的人渣一樣。好過分的詆毀。

⋯⋯絲特莉是不會說謊，不過有時候會表現得很誇張。

「總之，這件事就不要說了吧」

「嘛，也是呢。反正也收集到不錯的數據了⋯⋯我們還是想想剩下的寶具要怎麼辦吧」

絲特莉輕易就地停下假哭，擦拭眼淚開始思考接下來的事情。
這轉變之快使得緹諾表情是一陣痙攣。

我是因為習慣了，所以不覺得有什麼。不過對於緹諾而言，絲特莉不是她的師傅說不定是一件很幸運的事情。

「嘛，『結界指』也有些儲備了，問題是能夠維持到露西亞回來嗎⋯⋯」

加上還沒使用的結界指，總共是有八個。由於我有十七個，可以說有一半是充填好了。
需要提防的『虛空之塔』也已經完了，只要我多加留意一下，不要靠近危險的地方就行了吧。

更重要的是再繼續產生傷亡，我感覺自己應該會被罪惡感擊垮（露西亞那邊就只能讓她忍耐一下了）

「剩下的就交給雇傭人員也不錯。我知道一些口風較緊的地方，還能採取到數據。錢由我來出」

絲特莉拍了下手，面帶笑容地說道。
數據會不會收集太多了，收集那麼多是要幹嘛啊。

這時，由於絲特莉在場使得身體蜷縮起來的緹諾，她像是下定決心一般開口說道。

「⋯⋯那個、Master！如、如果不介意，我⋯⋯⋯⋯我來幫忙」

絲特莉睜大眼睛望著緹諾，而我也是覺得意外。

「⋯⋯誒？你是指充填？」

倒下的魔導師當中，應該有人比起緹諾的等級還要高的。
看見他們束手無策地嘔吐並失去意識的樣子，即便是為了師傅的親友也不會說出這種話吧。這究竟是什麼毅力，讓人不禁覺得她會不會太努力了。

以獵人來說，這也許是好的傾向，不過我可不想看見後輩嘔吐的樣子⋯⋯⋯

緹諾以膽怯的步伐走過絲特莉的身旁，來到我辦公桌的前面後，她雙手按在桌子上，眼含淚水地大喊。

「我、我也是一名獵人，Master！雖然沒有魔力，可如果需要的是毅力⋯⋯⋯⋯我會忍耐的！」

我覺得多半是做不到的。
聽說魔力回復藥的味道極為苦澀。就連習慣它的魔導師都是那副慘樣。
即便平時總是受莉茲過分的對待，以至於毅力變強，可還是有極限的。

「不，你的好意我心領了，不過其實不用的。這又不是緹諾的工作」

「怎麼會⋯⋯⋯⋯請、請讓我做！Master！我也做得到的！」

「誒！？」

緹諾眼含淚水，語帶悲痛地說。
她為什麼就這麼想嘗試地獄般的痛苦呢，我是完全搞不明白。

也許魔力是會提升，不過從根本上講緹諾又不是魔導師。
當然，魔力增強總比沒有要來得好⋯⋯她會勇敢面對遠比自己還要強大的格雷姆，是因為莉茲就是這樣教育她的嗎。

我是嚇得不輕，而緹諾是沒有打消念頭的樣子。她甚至沒有察覺到絲特莉正逐漸靠近她背後。

隨後，絲特莉滿臉笑容，大聲叫喊並抱著那無防備的後背。

「好⋯⋯好可愛！」

「噫！？」

在緹諾回頭之前，她的雙手就已經被封住，身體在絲特莉的緊抱下無法動彈。絲特莉這手法是十分熟練。
這又是再現剛剛休息廳發生過的事情，也許絲特莉就是百合。看她那淫蕩的笑容，我已經沒辦法再否定這件事。

「克萊伊，你看看！這孩子在跟你撒嬌耶！跟姐姐好像！不過實力還不夠，速度也不快，比起姐姐還要──可愛得多！」

「請、放開我！絲特莉姐姐！呀啊⋯⋯！」

對於她們姐妹倆的關係我是感到疑惑。而且你這已經不是疼愛了吧。

絲特莉不顧死命掙扎的緹諾，硬是用雙手將她從我眼前的──桌子上拉開。
一瞬間，緹諾臉頰就泛紅了。我不知道那究竟是因為害怕還是因為快感。

「克萊伊！請你看一下！緹諾的弱點是腳下！沿著腹股溝到大腿內側的血管來回撫摸，她就經不住了！」

「──！！」

緹諾身體一抖，發出無聲的尖叫。她身體挺得直直，裸露的白皙脖頸上流下一滴汗水。
當看到玩弄後輩的身體，並紅著臉訴說的絲特莉，我是嚇得要死。

你告訴我這件事是要怎麼樣？嗯？你以為我會覺得「是嗎，那下一次我也來試試看」麼？
不會的。不會的。腹部溝就是那個隱私部位吧？　你究竟把我當成什麼樣的人了。

緹諾的雙唇裡發出不成聲的叫聲。絲特莉將手指抵在她那伸直的脖頸上，眯細著眼睛，以宛如欣賞藝術品一樣的恍惚表情說道。

「嘛，可惜就跟克萊伊說的一樣，緹諾不適合那種試練⋯⋯」

「誒！？」

「歸根究底，如果誰都能做到──那我也不會拜託別人，自己就可以充填了⋯⋯對吧？」

緹諾發出細微的疑惑聲。
絲特莉並沒有放開緹諾，而是繼續語帶熱情地說道。

「小緹你似乎不知道，那我就告訴你吧。魔力的『負擔』很嚴重哦？一般來說，魔導師只能繼續作為魔導師度過，不然會面臨身體能力或五感精度在這兩種擇一的局面。如果只是稍微提升魔力那倒還沒有問題，可要是反覆進行那種試練，你以『盜賊』鍛鍊出來的身體──就會完全荒廢」

嘿，是這樣嗎。難怪沒有魔法劍士。真是學到知識了。

絲特莉的興趣是讀書與實驗。從以前開始她就是頭腦靈活的孩子。無論問什麼，她都可以答上來。即便不知道，下次相見時也可以得到答案。

真不知道她為什麼會變成現在這樣子。

「所以說⋯⋯明明樓下除了魔導師以外還有許多其他的成員，可我還是只讓八個人參與。吶？我有好好考慮過對吧？畢竟這個氏族的規矩是『大家友好相處』，而氏族整體戰力的降低也不是克萊伊希望看到的。我姑且也是有考慮過後果再做的哦？」

⋯⋯還好我有設定規矩。

絲特莉低聲說明的同時，還用似乎碰得到似乎又碰不到的手法撫摸著剛剛她自己說的『弱點』

錯就錯在緹諾還是穿著平時探索時那件短褲。也許是重視行動性，大腿裸露大片肌膚的服裝根本無法阻止絲特莉的手指。

每當指頭磨蹭大腿內側時，緹諾的身體就猶如遭到雷劈一般顫抖不停，白皙通透的肌膚也泛起紅潤，雙唇之間也屢次發出不成聲的細微呻吟。

她像是不情願般左右搖頭。
絲特莉用手指取掉她脖子上滴落的汗水，面帶妖艷的神色將其舔完。那笑容隱約給人一種威懾感。

「小緹⋯⋯你還聽得見嗎？你以為我偷工減料了？你以為自己沒道理不去幫忙充填？　雖然我有時候是會失敗⋯⋯可是不會偷工減料哦。就像小緹在探索寶物殿時⋯⋯不會偷工減料一樣」

「⋯⋯我覺得你差不多偷工減料比較好吧」

我不再逃避現實，插嘴道。
這不是應該在我面前做的事情吧？當然，我好歹是氏族會長，還是八級獵人，不會渣到對親友的弟子下手，不過我姑且是男人耶？
不是⋯⋯我的意思也不是別在我面前做就行。

你們老是這種無防備的樣子，要是讓伊娃看見，她又要瞪我了啦。
本來我就不怎麼幹活，所以還是希望減少別人對我的壞印象。

我以鋼鐵精神提出忠告，而絲特莉對此是微微點頭，在緹諾耳邊嘀咕。

「撒嬌是可以⋯⋯不過克萊伊很忙的。如果有不懂的事情就來問我，我會告訴你的。吶？　懂了吧？」

「⋯⋯⋯⋯好、的」

雖然我覺得這也不是撒嬌⋯⋯⋯

聽到緹諾的低聲回答，絲特莉終於放開手。也許是雙腿使不上力氣，緹諾的身體一陣踉蹌。而絲特莉在緊要關頭支撐住她的身體。
緹諾現在是氣喘吁吁。眼淚、口水、汗水使得臉蛋是一塌糊塗。在莉茲的特訓當中，她也是屢次失去意識，究竟在哪一邊會好一些呢。

絲特莉以天真無邪的開朗表情望著我。

「她這樣子有些危險⋯⋯我去讓她洗個澡冷靜一下。也許會花上不少時間」

「⋯⋯絲特莉⋯⋯⋯⋯我說你，該不會連女孩子也是可以的？」

聽到我忍不住提出的疑問，絲特莉頓時是眨眨眼睛，不過立刻露出微笑。

「如果──真有這個『必要』的話」

我沒想到她會這樣回答，後輩的節操有危險了。
回想起她剛剛露出的恍惚表情，不由得讓我按字面意思去理解這句話。

拜託，誰來阻止一下她。

「⋯⋯現在應該沒有必要吧。只要讓緹諾洗洗澡，感覺應該就沒事了，所以不用多久我就立刻回來。不然就拜託格雷姆？」

我重新搭著腿，隨意拿起桌子上的一份資料。
儘管我裝作淡然的樣子，不過背上狂流冷汗。不妙。

如果不小心讓絲特莉跨過那一條線，那就是我的責任了。只有這件事絕對要避免。
絲特莉看見焦急的我，微微蹙起眉頭。

「克萊伊，格雷姆在看不見的地方是很難給出這麼細致的命令。我會轉為研究生物兵器也是因為這樣」

「⋯⋯⋯⋯⋯⋯哦」

限制意外地麻煩。我還以為格雷姆是什麼都可以做的方便物體。
不過這種事怎麼樣都無所謂。再這樣下去，緹諾會被她帶回家的。
我可不能讓朋友成為罪犯。

「⋯⋯⋯⋯我明白了。雖然這是艱難的領域，不過只要進展順利，的確是很有用。反正研究室倒閉以後，瑪娜源相關的研究也都暫時停止了⋯⋯就讓我稍微思考──」

這時，從後面聽到一陣聲音。
隨著喀啦一聲，窗戶被打開了。吹進來的強風將整理好的資料吹飛。

我不知道說了多少次要來就從樓梯來。如果是平時我還會嘆氣，不過現在時機是剛剛好。
讓姐姐去應付妹妹是最好的選擇。

「克萊伊，早上好！嗯？絲特莉，你為什麼也在？難不成大家都回來了？」

莉茲以熟練的動作入侵房間以後，睜大眼睛望著絲特莉。
而被人支撐著的弟子，她看都不看一眼。

絲特莉完全不介意自己的犯罪現場讓人看見。姐姐的出現反而使得她眼睛熠熠生輝。

見狀，我領悟到是我自己太天真了。
現在我只能說已經不行了。