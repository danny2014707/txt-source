哥隆子爵看了金格。眼神在說，想想辦法吧。

金格是被王授予騎士位的敕任騎士，其所屬為瓦茲洛弗家。就算引退了，也不代表他不再是瓦茲洛弗家的騎士。最重要的是，他是一心忠義於瓦茲洛弗家而活的騎士，那就當然會遵從瓦茲洛弗家當主之意來勸說吧，子爵大概是這麼想的。更別說，哥隆子爵和金格是舊交了。但是，金格道出的，是讓子爵驚愕的話語。

「若要違反吾主之意，強行帶走，必將以命抵抗」

宛如是在與敵人對立的措辭。但是，金格的感情的溫度之高，是反映了主君諾瑪。也就是說，哥隆子爵有讓諾瑪如此憤怒。
既然如此，金格的所屬是瓦茲洛弗家這件事，也有疑慮了。

伴隨出嫁之女的騎士是所屬於原本的家還是婚家，是微妙的問題，此場合也很類似。在出嫁之女死去時，伴隨的騎士回到原本的家的情況很多，但也有繼續在婚家侍奉孩童的情況。那騎士的薪水要由哪一家來支付，也是各式各樣。簡單來說，會依照場合有所不同。

哥隆子爵展現了符合老練外交官的轉換。從沙發站起，雙手碰胸，垂頭謝罪。
那謝罪，是宛如在將身為工庫魯家之繼承人的諾瑪當作瓦茲洛弗家的人的謝罪。

諾瑪也沒打算對子爵窮追不捨，爽快地接受了謝罪。
然後，子爵再次邀請諾瑪去瑪夏加因。

但諾瑪宣告，現在正埋頭於對自己來說極為重要的事業之中，就算是瑪夏加因侯爵的邀請，也沒有外出到遠方的時間，因此要拒絶，如果是就算這樣也必須去瑪夏加因的急事的話，不告訴她那是何事，就沒辦法判斷。

子爵便說明，並沒有被允許揭露要事之內容，這是對這国家的數個高位貴族家而言，無法抽身的重要問題。
諾瑪詢問，那問題跟工庫魯家有什麼關係嗎。
子爵只能回答，沒有關係。

像這樣徹底地動搖子爵，確保交涉的主導權，諾瑪同意了，附有條件來訪問瑪夏加因。
那條件，是要將離開沃卡並回到沃卡的時間安排為三十天以內，以及發誓沒有諾瑪的同意就絶不會要她做些什麼。
哥隆子爵當場發了誓後，諾瑪便快速展開了行動。立刻命令芬汀為旅程準備，和抄寫師拉庫魯斯說明以及討論外出時的作業，並在隔天從沃卡出發。

同行的有金格、芬汀、艾達和一名侍女。
艾達自己志願了同行。對諾瑪來說，艾達同行的話也很讓人放心，只要有藥聖的話語，就算是瓦茲洛弗家，應該也不會拘束艾達吧。

最初原本是要伴隨三名侍女的。諾瑪認為不需要侍女，而且瓦茲洛弗家也會派出照料諾瑪的起居的侍女。但普拉多和肯涅爾以這是跟家的體面有關的問題為由而不肯退讓，沒能拒絶只帶一人。此外，諾瑪不會坐瓦茲洛弗家派來的馬車，而是坐工庫魯家的馬車。也伴隨了一名車夫。

瓦茲洛弗家雖然派遣了兩名騎士，但年長的騎士是金格過去指導過的人物，深深尊敬著金格。年輕騎士看向金格的眼神也充滿敬意。因此，一行人在金格的指揮之下，順利地旅行了。有被數隻蜘蛛猿襲擊過一次，也有被盜賊襲擊，但都被艾達以〈伊希亞之弓〉擊退，人員和馬車都完全沒受到傷害。兩位騎士得知了〈藥聖之治癒師〉是高手冒険者，感到非常驚訝。瓦茲洛弗家的侍女也沒有多管閒事，而且做事周到，諾瑪在路途中完全沒有感受到不快。

路途中，諾瑪在瓦茲洛弗家的馬車上度過了許多時間。想在旅程時，從哥隆子爵身上盡可能地得到，瓦茲洛弗家與其周邊的相關情報。
馬車花了十四天抵達瑪夏加因。

諾瑪被帶領到了客棟。是非常好的房間。準備了澡堂，提供了晚餐。
然後在隔天，諾瑪和瓦茲洛弗家當主曼夫利會面了。金格、芬汀和艾達同席了。

曼夫利今年四十一歳。儘管是堂兄，也比諾瑪大了十四歳。

「工庫魯家繼承人，諾瑪・工庫魯殿，百忙之中，接受了吾之請求而來訪，實在非常感激。久違了啊，諾瑪」
「是。好久不見了，曼夫利大人」