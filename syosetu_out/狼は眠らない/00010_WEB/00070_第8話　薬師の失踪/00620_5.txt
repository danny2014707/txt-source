５

「哎呀哎呀，真是豪邁的吃相。畢竟才剛從迷宮回來呢。在這種時候碰巧與少爺一行有所衝突，真不知道該說運氣是好是壊。」

受傑尼招待來到餐廳的包廂，雷肯正打從心底享受著食物與酒。迷宮之後的飯，果然美味。而且是這種高級餐廳的料理，更能深深體會到活著的喜悅。

「那麼，既然發生了這種事，就不能不告訴雷肯先生來龍去脈了。」
「不說也行。」
「領主大人將夫人迎來時，夫人的同輩親戚的騎士，米多斯可・亞邦克連大人，也一同來到了這城鎮。這位大人非常奢侈浪費，而且明明能力不高，卻很有權力慾，佔據著各種職務和權利，甚至還企圖讓自己的長男亞利阿大人坐到沃卡領主的位置上。」
「再一瓶這個酒。」
「好的。」

傑尼回應雷肯的要求，叫來了服務員點酒，並繼續話題。今天房間裡沒有部下待機。似乎是考慮到讓雷肯好好放鬆，但好像不只如此。

「與米多斯可大人聯手的有兩人。一人是，大商家寨卡茲商店的當主，札克。另一人是，貢布爾領主次男，亨吉特・道加大人。」

雷肯又吃完一個炸雞腿，把剩下的骨頭丟掉，接著在杯裡倒滿酒，大口飲下。

「寨卡茲商店在這城鎮開了支店，長年來慢慢累積著販售實績，用了些許違反道義的方式打垮競爭對手，滲透進了城鎮的中樞。最終，當主札克自己來到這城鎮，我的商店很遺憾地，被踢下了領主館交易商人首席的位子。」
「這個炸雞腿，再一盤。」

點完料理後，傑尼繼續說道。

「同時，領主大人一點一滴地，從不滿米多斯可大人的做法的，以及受到背叛的人們身上收集證言與證據，伺機斷罪的機會。」
「蔬菜也想吃。點些蔬菜吧。」

點完追加的後，傑尼繼續說道。

「米多斯可大人自詡為這座城鎮的武力方面的負責人，從以前開始，就一直要求領主增員騎士與士兵。但領主大人以，騎士只需現在的四人就很充分，要讓士兵增加到六十人以上也有預算上的困難，因此沒有必要，這樣的理由拒絶了。」
「騎士只有四人嗎？」

雷肯邊喝著酒邊詢問。

「是的。領主大人與之長男，米多斯可大人與之長男，共計四人。」
「領主的長男後頭不是有兩個看起來像騎士的人嗎。」
「那只是裝備得像騎士而已。要成為正式的騎士需要王的裁定許可。」
「原來如此。」
「由於訴求增強武力的米多斯可大人實在太過囉嗦，領主大人便拜託希拉大人，跟王都的知名魔導具技師，鴉庫魯班多大人，購入五支能擊出火焰魔法的魔導具。」

這麼一說才想起來。其中一支正放在雷肯的〈收納〉之中。

「一名能擊出強力火焰魔法的魔法使，據說能與十名騎士匹敵。這代表增強了騎士五十人分的戰力，米多斯可大人也只能住嘴了。但接著又以各種理由，要求由自己來管理那魔導具。領主大人，讓米多斯可大人負責保管五支中的兩支。」
「麻煩再一瓶酒。」
「前些日子，領主大人令嬡，絲夏娜小姐得了病。雖然讓城裡的優秀施療師們診察了，但病名和原因都查不出來。領主大人束手無策，只得乞求希拉大人出診。希拉大人說，這是源於詛咒的病，普通的藥治不好。」

源於詛咒的病，就算治了，也只會再得一次。不解咒就無法完治。

「這時，米多斯可大人和札克・寨卡茲自信滿滿地提議了。只要交給我們負責，一定能找到拯救小姐的藥。不用說，會要求不得了的回報。」
「米多斯可知道那是詛咒嗎？」
「誰知道呢？生病一事應該有傳達吧。就連夫人也不知道詛咒的事。總之，得知了這件事後，我如此提議。我會找到藥的，這樣。」
「喔？」
「其實，在某個村子裡有個小小的迷宮，十多年前，出現了〈神藥〉。那是村長的兒子為了治療母親的重病，下定必死的決心才得到的奇蹟的藥。然而最後沒能趕上，母親去世，兒子也悲傷地離開村子，在別的迷宮喪命了。村長在那之後，沒有告訴任何人這件事，偷偷地藏著藥。」
「但你知道嗎。」
「以前曾將那村子從危機中拯救出來呢。作為回報，打算交出〈神藥〉。當時我拒絶了。因為價值相差太多了。這次，我準備了最大限度的金錢，並提出交易收購了〈神藥〉。雖然盡可能地不引人注目，除了車夫外只有兩名護衛，以這樣的少人數去了，但反而有了反效果。」

原來是因為這種情況才有那種護衛募集嗎，雷肯想著。

「希拉大人判斷的，小姐能活到的最大限度的一天，是把藥帶回來的期限。至於之後的事，雷肯先生也知道。啊啊，對了對了。我將藥送到領主館的隔天，亨吉特・道加大人突然來訪了。說是，耳聞小姐的危機，帶了絶對能治好病的藥來了。」
「意思是米多斯可和亨吉特・道加是同夥嗎。」
「是的。」
「那麼，如果拜託米多斯可和札克，到時候亨吉特就會來嗎？」
「雖然是這麼認為的，但這方面也不太清楚。」
「對亨吉特來說有什麼利益？」
「亨吉特大人從以前就愛慕著絲夏娜大人。」
「原來如此。」
「那麼，接下來的都是傳聞與推測。首先，有傳聞說，米多斯可大人遺失了保管的魔法武器。」
「喔。兩個都是嗎？」
「兩個都遺失了。那是是用毫無道理的理論，逼領主大人交付保管的重要的武器。而且是花了大錢才買到的。不是說個，我弄丟了，就能放過的。米多斯可大人訪問了希拉大人好幾次並說，強迫我們買下的武器壊掉了，盡速送來代替的物品，如此要求著。」
「為什麼不找領主。」
「領主大人購買武器時，根據製作者的要求，在誓約書上署了名，上面寫著，不論壊掉了還是遺失了，都絶對不能再申請購買。」
「所以希拉之所以失蹤，跟這有關？」
「我是如此推測的。現在，城鎮裡的主要藥屋正抗議著，希拉大人承受不住米多斯可大人無理的要求而離開了。當然，作為藥屋貴客的城鎮裡的有力人士們，也對米多斯可大人深感憤慨。與這些人士對立的米多斯可大人的陣營的人，正相繼逃跑著。」
「札克・寨卡茲也是嗎？」
「是第一個跑掉的呢。總而言之，向米多斯可大人問罪的機會正日漸成熟。不對。是已經幾乎要成熟了。」
「原來如此。話說回來，妮姫指得是誰？」
「喔呀？還沒有見過嗎？是希拉大人的孫女喔。雖是位年輕女性，但曾消滅這城鎮附近出現的強大魔獸兩次，因而晉級為金級冒険者。」
「希拉的孫女？年輕女冒険者？該不會是，腰上掛了細劍，看起來年齡有二十左右，會讓人覺得希拉年輕時就長得像這樣的，黃金色頭髮的女冒険者？」」
「沒錯，沒錯。」
「是嗎。雖然不知道名字，但有一起採集藥草二十天左右。」
「只有兩人嗎？」
「啊啊。」
「那別讓少爺知道會比較好喔。」
「為什麼？」
「畢竟所謂戀愛是盲目的。」
「喔？戀愛？對那個？」
「前幾日，妮姫小姐有在這城鎮的冒険者協會露面。是有誰告訴了阿基托大人吧。跟希拉大人失蹤的騷動混在一起，誇張地。」
「原來如此。」
「話說回來，雷肯先生。這次的迷宮如何呢？」
「突破到最下層了。迷宮之主掉落了這個。」
「這，這是！第一次見到呢。是金色藥水對吧。能把這個賣給我」
「不能。我要自己用。」
「說的也是呢。」
「這次寶箱沒有給別的東西。加上一些上次多出來的，賣你一些藥水。」
「喔喔！太感激了。」

雷肯事先在袋子裡裝入了青藥水和紅藥水所有的中和小，帶了過來。
交出袋子，最後一口氣喝光剩下的酒，雷肯從座位上站起。

「很美味。」
「那真是太好了。」