「⋯⋯嗯？馬車停了」

平穩行駛在正路上的馬車突然停了下來
附近也沒有什麼特別的東西，這是要幹什麼呢，這樣想著，車棚被掀開，亞蘇爾探頭進來

「對不住，老爺們⋯⋯我要去辦點事」

原來如此，雖然不知道是什麼地方派來的刺客，但畢竟還是人
不過一定也不只是去『辦點事』吧⋯⋯

我還在裝睡，所以羅蕾露回答

「雖說街道附近很少有魔物出沒，但說不定會有盜賊，要去的話⋯⋯」

意思是說，最好有人陪著一起去
這是一般的做法，之前也都是這麼做的
但亞蘇爾巧妙地接過了羅蕾露的話

「不不不！怎麼能讓女人跟著我呢！這也太羞恥了⋯⋯！！」

一邊這樣說著，趁著別人，主要是趁著奧格利還沒插話，趕快溜走了
他就照自己所說的那樣去辦事了，怎麼可能呢
若是真的，帶奧格利一起去明顯會更加安全
他就是為了防止發生那種事，才會特意挑選剛才的時機搭話吧
確認我睡著了，向離得最近的羅蕾露搭話，然後利用她的回答
失敗的話，他也一定還準備了其它藉口

「⋯⋯走了吶，哎呀哎呀，大概是被我看到也很羞恥吧」

奧格利聳聳肩，開起了玩笑

「要說想不想被你看到的話，一定是不想的吧。而且你這身打扮，在森林裡也有點顯眼過頭了」

奧格利現在也是一如既往的浮誇打扮
實際上，襲擊過來的魔物也往往會被他的打扮吸引，大多數都會朝那邊涌過去
若那身打扮是考慮到了這點，他也算得上是個捨己為人的冒険者了吧，但據本人所說，這只是出於個人愛好罷了
效果什麼的沒有特別想過
不過他也可能是不想讓別人覺得對不起自己，才特意這樣說的吧

「如果有必要進行隱藏的話，也可以在外面再披上一件斗篷什麼的⋯⋯嘛，現在這不重要」
「果然他是去和森林裡的同夥匯合了吧，那麼⋯⋯要不要去跟蹤一下？」

羅蕾露這樣問到，這可是個容不得大意的問題

「三個人一起去的話，一定會被發現的⋯⋯就我一個人去好了，如果可以的話就把對方一起抓起來」

無論誰去都無所謂，但亞蘇爾前往會合的同伴中，也許會有感知能力很強的人
如果是我，在很遠的距離就能進行跟蹤了
這也是拜魔物的身體能力所賜

「那就靠你隨機應變了。不過，最重要的還是弄清楚對方的目的和人數，就算放任他們不管也可以。如果勉強想要捉住，卻令對方逃脫了，反而會比較麻煩」
「先弄清楚對方的手牌，再進行收割麼？做這一行的人碰到你簡直就是遇到了噩夢啊⋯⋯」

奧格利一臉戰慄表情地說到，不過這只是在開玩笑吧
雖然說得也沒錯
不知道對方是什麼人，但在他們的立場上看，無論採用什麼手段都沒有效果，確實會像是噩夢一樣吧
我也很讚同

「就這麼辦吧」

我點了點頭，隱藏起氣息，悄悄離開了馬車


◆◇◆◇

「哥布林」是相當有本事的特工

但即便如此，也不可能憑一個人就完成全部任務
因此，為了能夠在發生意外時提供幫助，這次也有同僚被一起派了過來
話雖如此，其實這是主人出於以防萬一的考慮，派遣了稍微過剩的戰力
但是，「哥布林」認為，這次主人的判斷是正確的
離開馬車進入森林，裝作沒事的樣子，尋找著只有同伴才看得明白的符號
沒過多久，他便來到了兩個人身邊

「哥布林」開口對他們說到

「⋯⋯計策失敗了，但還沒有暴露，行動還將繼續」
「⋯⋯沒想到你竟然會搞砸啊，『哥布林』，這次的對手有這麼厲害麼？」

是一個年輕女子的聲音妖艷嫵媚的聲音，充滿了自信的感覺

「不⋯⋯這還很難說，可能只是對方運氣好」
「那再嘗試一次不就行了？」

女人很乾脆的答道，但有些欠考量
她也是和「哥布林」受過同樣訓練的特工
但經驗尚淺
加入才不過短短數年
任務也只完成過一些比較簡單的
所以，她還沒能明白，世上存在著難以理解的事物這一點

現在，「哥布林」也不過是稍微感到了一點違和感，但內心的某處告訴他，這次的目標絶對容不得大意
雖然想來想去還是覺得是自己多慮了，但這種時候，相信直覺往往會更好

「⋯⋯『塞壬』①，我知道你的光輝經歷，但這一次的對手恐怕會超出你的常識。要時刻保持這種意識，當然，也可能只是沒什麼大不了的對手罷了」
「誰知道呢？嘛⋯⋯在那之前，我需要先整理好自己出場的『舞台』」

說完之後，「塞壬」便不再出聲
和「哥布林」他們說話的，是一個和「塞壬」類似，但又比較沙啞的聲音

「⋯⋯雖然不能輕敵，但也不要太過恐懼對手了，『哥布林』」
「沒錯，但是」

總絶得有些不安
心裡有一種隱隱約約的違和感⋯⋯

稍微有些心神不寧的「哥布林」苦笑了一下

「⋯⋯別擔心。如果出了什麼問題，我就幫你善後吧吧，『哥布林』，你和『塞壬』⋯⋯就先放手試一試吧⋯⋯」

說完這句話，對方的氣息消失了