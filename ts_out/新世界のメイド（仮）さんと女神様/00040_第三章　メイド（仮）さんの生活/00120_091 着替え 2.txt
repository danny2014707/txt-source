繫帶內褲的問題暫時解決了。胸罩方面也只是用帶子來代替前扣式的形式，並沒有什麼特別令人煩惱的地方。在沒有任何問題的情況下，在那上面穿上了一層薄薄的淺青色的──形狀和顏色都很好看──襯衫。

接著，就是將買來的吊帶襪和長筒襪──不是用卡扣而是用帶子連接──穿好，穿上牛仔褲繫上腰帶。牛仔褲前面的開口處不是拉鏈而是扣子。不愧是緊綳綳的編織物牛仔褲，下擺塞入鞋中根本就是不可能的，一隻一隻穿好鞋子，將下擺覆蓋在上面就大功告成了。

「噢噢，這樣的姿態意外的十分搭配呢」
「是嗎？　謝謝」

麻里子用笑容回應了米蘭達的贊詞。可是，儘管一心想要擺脫裙子，但麻里子對現在穿的衣服卻沒有安全感。

（普通的衣服是這樣的感覺嗎？與其說是感覺上防御力低，不如說是完全沒有）

穿著睡覺時的浴衣也有類似的感受，但畢竟也就是睡衣，麻里子也沒有多在意。可是，就算是將衣服穿的更結實，這牛仔褲的感覺也和之前那一模一樣。

（女僕裝沿用的是遊戲中的設定，是這麼一回事麼。那麼，在戰鬥中還是穿著會比較好嗎）

視線落到剛剛被脫掉放到一邊床上的女僕裝，麻里子這樣想著。


◇

把要洗的東西丟進道具箱，正整理著剩下東西的麻里子將臉轉向米蘭達。

「啊，對了。有點事想問米蘭達桑⋯⋯」

雖然是這麼說，但麻里子卻在中途停了下來。

（等下等下，就算我的記憶多少有些問題，問出要怎麼把行李裝入壁櫥這種話聽起來也太可笑了吧）

「是什麼，麻里子殿。不是有想要問的事情嗎？」

米蘭達以一臉詫異的表情看向話在中途斷掉的麻里子。

「哎，誒都。那個，塔莉婭桑。塔莉婭桑現在在哪裡知道嗎？」
「塔莉婭大人嗎。這個時間應該是在辦公室吧。你想，前天麻里子殿來的時候，我端茶進入的那個房間。大概，就在那裡看文件呢吧」
「啊啊，是那個房間嗎」

那是突然想到的名字，剛剛想要問的問題只要問塔莉婭就可以了，麻里子這樣想到。

（要是某種程度上了解情況的塔莉婭桑，就算我問的問題有些怪異也不會覺得奇怪吧）

「麻里子殿打算去塔莉婭大人那裡嗎？」
「是，有點事想要打聽一下。還有也想感謝一下準備金的事」
「啊，要這麼說的話，在中午的時候去食堂見面不是會更好嗎？」

米蘭達很少見的壓低了聲音。

「哎？　為什麼？」
「現在去我想就會被捉住的」
「被捉住？」
「說了是在做看文件的工作吧？　會被捉住被迫來幫忙的」
「不會吧。也不可能將文件之類的給剛過來的人看」
「到底是會怎麼樣呢，總之，麻里子殿。塔莉婭大人好像非常看好麻里子殿呢。」

露出一副很高興表情的米蘭達，挽著胳膊看著麻里子。尾巴輕輕地擺動著。

「請別嚇我」
「總之就是這麼一回事，要去的話一定要當心」
「明白了」
「那麼，我先會房間休息片刻。之後在食堂見」
「好」

說著些要是沒做食堂看到我有可能是在房間裡睡過頭了請上來叫我一下之類的話，米蘭達離開了麻里子的房間。


◇

總之將剩下的行李裝入道具箱的麻里子離開了房間。塔莉婭的辦公室在旅館的西北角上。從麻里子的房間，一直向右走到走廊的盡頭。在辦公室前的麻里子敲了敲門。

「在」
「塔莉婭桑，是麻里子」
「哦呀，請進。進來吧」
「失禮了」

麻里子進入了房間，塔莉婭同前天一樣從桌子上抬起頭。看到麻里子的樣子，一副十分驚訝的樣子。

「啊啊，去買東西了啊。即使那樣也是選擇了相當質樸的東西呢。沒被薩妮婭抱怨嗎？」
「你明白的吧」
「那倒是。所以怎麼了？」
「是的，首先準備好錢這件事十分感謝。托你的福終於能換衣服了」

麻里子說著禮貌的話深深地低下頭。

「好啦。這個也是工作份內的事。而且還預定要換回來呢」
「儘管如此」
「嘛好啦。吶，說了首先，那接下來是？」
「誒都，關於買回來的東西⋯⋯」

麻里子說出了關於收納方面的疑問。

「啊啊，那是薩妮婭的疏忽。總是把衣服往行李或是衣櫃裡一丟完事，老是在這點上疏忽吶。」
「果然，並不是說要將所有的東西都直接放到壁櫥或是道具箱裡麼」
「當然了。嗯，啊啊，你是在那一方面不了解才特意來問我的嗎？」
「就是那樣」
「那麼，就順便說明一下吧」
「拜託您了」
「一般來說，有必要隨身攜帶的東西都會放在道具箱裡，而其他的東西則是放在家中。就算是道具箱，也是有存放極限的。探險者那一類人，大部分是沒有固定居所的，其中很多人都會將所有的財產放在道具箱中隨身攜帶。而且他們之中，在進行真正探險的時候也會把行李寄存在旅館之中」
「原來如此，大致明白了」

塔莉婭的說明，大致上就如麻里子所想的一樣。那一方面的感覺好像和在日本或是遊戲中沒有什麼差別。

「行李只是恰巧忘記放進壁櫥裡了而已，要是薩妮婭應該會說出這樣的話吧。這些就夠了嗎？」
「是，非常感謝」
「不不，不用那麼客氣。只是薩妮婭的遺漏罷了」

塔莉婭那樣說著輕輕地擺了擺手，認真的看著麻里子。

「那麼，要問的事情都完事了嗎？」
「是」
「對了麻里子」
「是」
「現在，手頭有空嗎？」