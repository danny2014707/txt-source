---
LocalesID: 新世界のメイド（仮）さんと女神様
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%EF%BC%88%E4%BB%AE%EF%BC%89%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98.ts
---
__TOC__

[新世界のメイド（仮）さんと女神様](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%96%B0%E4%B8%96%E7%95%8C%E3%81%AE%E3%83%A1%E3%82%A4%E3%83%89%EF%BC%88%E4%BB%AE%EF%BC%89%E3%81%95%E3%82%93%E3%81%A8%E5%A5%B3%E7%A5%9E%E6%A7%98.ts)  
總數：47／47

# Pattern

## 麻里子

### patterns

- `マリコ`
- `麻里子`
- `麻理子`

## 亞里亞

### patterns

- `アリア`

## 卡米魯

### patterns

- `カミル`
- `卡密魯`
- `卡米爾`

## 薩妮婭

### patterns

- `サニア`
- `薩妮婭`
- `薩尼婭`

## 塔莉婭

### patterns

- `タリア`
- `丹妮婭`
- `塔利亞`

## 哈薩爾

### patterns

- `ハザール`
- `ハザ—ル`
- `卡薩魯`
- `哈札爾`

## 拉茜

### patterns

- `ラシー`

## 吉娜

### patterns

- `ゲナー`

## 卡農

### patterns

- `カノー`

## 納薩爾

### patterns

- `NAZAR`
- `ナザール`
- `納賽爾`

## 米蘭達

### patterns

- `ミランダ`

## 席娜

### patterns

- `シーナ`

## 茱莉亞

### patterns

- `ジュリア`

## 瑪琳

### patterns

- `マリーン`
- `馬琳`

## 伊莉

### patterns

- `エリー`

## 普拉托

### patterns

- `プラット`

## 巴爾特蘭德

### patterns

- `バルトランド`

## 巴爾特

### patterns

- `バルト`

## 托爾斯頓

### patterns

- `トルステン`
- `托爾斯滕`

## 珊德拉

### patterns

- `サンドラ`

## 卡琳

### patterns

- `カリーナ`

## 米卡

### patterns

- `ミカ`

## 米卡埃菈

### patterns

- `ミカエラ`

## 阿朵蕾

### patterns

- `アドレー`

## 哈維伊

### patterns

- `ハーウェイ`

## 矢島

### patterns

- `ヤシマ`

## 哈基米

### patterns

- `ハジメ`

## 赫曼

### patterns

- `ヒューマン`

## 斯茲卡

### patterns

- `ツヅキ`

## 棕色虎斑

### patterns

- `野雞虎`
- `雉トラ`

## 赤虎

### patterns

- `赤トラ`
- `紅色的老虎`

## 迷宮

### patterns

- `ダンジョン`
- `地宮`

## 白化

### patterns

- `ホワイトアウト`
- `White Out`

## 牧羊犬

### patterns

- `コリー`
- `Collie`

## 最前線

### patterns

- `フロンティア`

## 獸人

### patterns

- `アニマ`
- `尼瑪`

## 門之看守者

### patterns

- `門の番人`

## 開拓者

### patterns

- `パイオニア`

## 探險者

### patterns

- `エクスプローラー`
- `探檢者`

## 物品箱

### patterns

- `アイテムボックス`

## 存物箱

### patterns

- `アイテムストレージ`

## 胖次

### patterns

- `パンツ`

## 尺寸

### patterns

- `サイズ`

## 胖次尺寸

### patterns

- `バストサイズ`


