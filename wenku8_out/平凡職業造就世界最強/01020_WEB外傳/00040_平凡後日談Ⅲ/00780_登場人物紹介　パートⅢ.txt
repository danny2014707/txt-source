次のお話のプロットが中々まとまらず、すみませんがもう一週かけさていただきます。
とはいえ、何もないのもあれなので、ただの登場人物紹介ですが投稿します。アフターⅢを振り返る參考にでもしていただければ！
よろしくお願いします。


════════════════════

≪希雅旅行編≫
・バンタス
旅行先で出會った村人。村最強の絶對王者。魔王特製ドーピングで過去最高の闘爭を希雅と繰り廣げ、村の傳說となった。

・ウルト
旅先で出會った村人青年。希雅に技をコピーされまくる。希雅を心の中で師匠と呼んでいる。師匠、來年も來ないかなぁと思ってる。

・ウィルフィード
三十代前半のイケメン。「人生はいつも斜め上」が口癖。表は貿易會社、裏では『力ある遺物』の捜索・研究・兵器轉用等をしているレリテンス社の非合法社員だったが、ハジメに見逃されてからは退社。現在は仲間と遺物關連の會社を立ち上げ、新進氣鋭の冒険家として徐々に知られ始めている。心はハジメの犬。呼べば來る。

・ブランドン
同僚のウィルフィードに『會話の邪魔』なんて理由で首コキッをされて、あっさり現世から退場した研究者。

≪フルールナイツ編≫
・萩原まち子
漫畫家『南乃みなみのスミレ先生』のアシスタントさん。四十五歳。スミレスタジオの最古參。愛稱はマチ姐桑。ハジメも、昔から世話になっているので頭が上がらない人。

・若井司わかいつかさ
同アシさん。二十四歳。少女漫畫家を目指すも、なぜか途中から必ず濃厚な格闘漫畫（グラップラー刃◯並）になってしまい、未だに芽が出ない。

・瑠璃河るりかわアンナ
同アシさん。米国人の母と日本人の父を持つハーフ。二十五歳。日本のサブカルチャーという業が、彼女を大學から自主退學させた。

・青山あおやま鳴海なるみ
同アシさん。四十二歳。貴腐人。愛稱ナルさん。最古參なので、ハジメはネッチョリした視線を向けられつつも未だに頭が上がらない。

・望月もえ
同アシさん。副業で女僕喫茶を經營。二十九歳。女僕服をこよなく愛しており、だいたい女僕服を著ている。警察によくお世話になるが、事情聽取も當然女僕服で受ける。姉妹店にウサミミメイド喫茶も經營。店員は全員特殊技能持ち。

・服部幸太朗
歸還者對應課（通稱、魔王課）所屬の疲れたおっ桑。歸還者と政府を繫ぐ窓口役を長期にわたり擔える剛の者。胃藥を飲むのが拔き擊ちより流麗で速い。南雲家BBQの肉をさりげなくパクリ、繆から目をつけられている。最近、頭頂部がさみしくなってきた。

・ヘリオトロープ
例のあの人。

・ネメシア
莉莉安娜のため（笑）の超人女僕集團『フルールナイツ』序列第二位。外殺の涅雅修塔特爾姆。またの名を郝里亞桑家の涅雅醬。ボスに侍りたい全ウサミミ女性達を死闘の末に打倒し、暗殺と諜報では某深淵卿に追隨するほどの手練れ。全ては、ボスの女に──ではなく、役に立つため。と本人は言っております。

・アイヴィー
フルールナイツ序列第三位。本名、芬莉。竜人族。緹奧の乳母だが、なぜか若返っており母性と色氣が淒まじい。代々庫拉魯斯家に仕える由緒ある一族なのだが、氣が付いたら魅惑の超人女僕になっていた。緹奧桑はわけが分からなかった。

・サルビア
フルールナイツ序列第五位。物資・情報管理を擔う。本名、薩米亞・庸凱爾。莫多・庸凱爾の孫。魔王陛下の嫁にしたいという下心で送り込んだが、氣が付いたら超人女僕になっていた。お祖父醬はわけが分からなかった。

・プリムラ
フルールナイツ序列第六位。本名、フィリス・ザーラー。愛子崇拝者である神殿騎士大衛・札勒の實の妹。元は心優しいシスターさんだったが、氣が付いたら超人女僕になっていた。お兄桑はわけが分からなかった。

・トレニア
フルールナイツ序列第七位。本名、特蕾西・Ｄ・荷魯夏。好戰的な帝国の皇女であり、以前は何かと莉莉安娜に對抗心を燃やしたり、會えば惡役令孃まっしぐらな言動を取っていたりしたが、氣が付けば莉莉安娜のため（笑）の超人女僕になっていた。莉莉安娜桑はわけが分からなかった。

・ベロニカ
フルールナイツ序列第四位。本名、庫潔莉・雷爾。海利希王国の騎士團長⋯⋯のはずだが、いつの間にか女僕になっていた。莉莉安娜桑はなんとなく察した。職場環境って大事だなぁ、とも思った。

・フリージア
フルールナイツ序列第十位。ハジメ謹製のメイドロボ！！メイドロボ！！なお、中身は某アラクネさんの中の人の片割れだとかそうでないとか。

≪深淵卿第二章・バチカン編≫
・遠藤真實まなみ　（※漢字を變更しました）

浩介の妹。中學一年の文芸部員。眼鏡とおさげがデフォルト。ソウルシスター。某後輩醬が魔王先輩に弄ばれてる光景を見て、ちょっと興奮しちゃうお年頃。加えて、最近、顏を合わせたアジズ君の浩介に對する慕いっぷりに、よからぬ妄想を膨らませちゃうお年頃。

・遠藤宗介そうすけ
浩介の兄。法學部の大學生。弟への嫉妬が止まらない。でも、弟から小遣いを貰うことに躊躇いはない。が、最近、某ドジっ娘聖女桑と顏合わせをし、嫉妬が天元突破したため貰った小遣いの半分を叩き返した。

・遠藤英和ひでかず　（※名前を變更しました）

浩介の父。四十九歳、市役所住民課勤務。趣味は釣り。どこか梅宮◯夫氏を彷彿とさせる。複數の女性との婚姻屆を出しに來た息子がいると、最近、職場で何かと話題にされる。

・遠藤實裡みさと　（漢字を變更しました）

浩介の母。四十九歳。市役所市民稅課勤務。最近、職場の同僚（おば樣方）にめちゃくちゃ息子の戀愛事情を聞かれるので、早期退職して息子と一緒に異世界に行ってしまおうかと考えている。

・克勞蒂亞・巴倫伯格
バチカンの對惡魔組織『オムニブス』最強のエクソシストで聖女。ただし、普段は重度のドジっ娘という自爆系聖女。走行中の車からよく轉げ落ちる。一度、飛行中のチャーター機から落ちたこともある。しかし、なぜかいつも無傷という、ある意味『聖女』の稱號に相應しい奇跡の存在。深淵卿に助けられ、兩親の仇を討つという宿願を果たすことができた。結果、深淵卿の四番目の嫁、かもしれない。追記：緹奧並のスタイルで、特にお尻が大きい。

・アジズ＝スタイン
オムニブスのエクソシスト。克勞蒂亞の義弟。克勞蒂亞ほどではないが、浩介に氣が付く良い勘の持ち主。半端なく浩介を慕っており、それを見た一部の腐った人達からは妄想の餌食にされている。

・ウィン＝キーマン
オムニブスのエクソシスト。金髮オールバックの二十八歳。真面目な性格で、克勞蒂亞をとても大事に思っている。が、彼女がドジっても八割くらいは見なかったことにする。限度があるのだ、何事も。

・アンナ＝フォーク
オムニブスのエクソシスト。栗毛三つ編みの十五歳。快活なトンファー使い。某SOUSAKANから惡い影響を受けたせいか、最近、トンファーを使わないトンファー使いになりつつある。

・パトリック＝ダイム
オムニブスの長官。物理で惡魔を殺す。眼光で浩介を殺しにかかる。神器である金屬製書物を讀まない。鈍器として使う。ついた二つ名は『撲殺神父』『書物の冒瀆者』『あいつ、實は信仰心とか全然ないんじゃね？』『惡魔絶對殺すマン』『奇跡の物理使い』『深淵卿によく降下強襲しかける人』など。時折、味方から惡魔に間違えられる。

・マーヤ＝コロカ
オムニブスの元エクソシストで、現在は秘密通路の管理人。物理で惡魔を殺す。眼光で浩介を殺しにかかる。神器である弓矢を放たない。鈍器又は刺突武器に使う。ついた二つ名は『惡魔絶對殺すウーマン』『弓の冒瀆者』『っていうか、もう普通に劍とか鈍器で戰えばいいんじゃね？』『對撲殺神父最終兵器』『最恐聖母』『深淵卿を見る目が常に笑っていない人』等々。時折、味方から惡魔に間違えられる。

・リー＝モーア
オムニブスのエクソシスト。中国と英国のハーフで、二十一歳の青年。ボーガン使い。エクソシストの中で一番、浩介と氣が合う。

・バッカス＝ルール
オムニブスのエクソシスト。禿頭のルール兄弟の兄。バトルアックス使い。日本のホラー映畫がトラウマ。

・ブルース＝ルール
オムニブスのエクソシスト。禿頭のルール兄弟の弟。古式ライフル使い。日本のお化け屋敷がトラウマ。

・シャリフ＝イースト
オムニブスのエクソシスト。眼鏡をかけたサラリーマンっぽい雰圍氣の中年の男。タワーシールド使い。優秀な盾役。實はドＭだが、顏には一切ださない特技の持ち主。

・キアラ＝ヴァッティ
オムニブスのエクソシスト。鋭利な目の三十代の女性。意識高い系キャリアウーマンみたいな雰圍氣だが、中身はメルヘンな人。カンテラ使い。

・Ｔ＝Ｊ
オムニブスのエクソシスト。彼、もしくは彼女。姉御と呼ばれる橫笛使い。少し前までは、正義感は強いがそれ故に少々暴走しがちな困った桑ではあるものの、普通の男性だったのだが⋯⋯日本で何かがあり、目覺めたらしい。

・教皇
實はオムニブスの總司令官という裏の顏を持つ。ダイム長官とは古くからの友人だが、胃にダメージを受ける時はだいたい長官が原因。最近は、深淵卿を窓口に魔王一派との折衝などもあり、加速度的に胃藥の量が増えている。一番偉い人なのに、一番の苦勞人。あと、長官とマーヤの二つ名の發信源はだいたいこの人。

・レダ＝ロッカ
オムニブスの元エクソシスト。救いのない戰いに心を折られ、惡魔に魂を賣り渡した。

・リットマン教授
某大學の宗教學の教授。『真實の神話』を知りたいがために、惡魔に魂を賣り渡した。

・チャンピオン
カンガルー型のグリムリーパー。ボクシング特化型。デンプシーロールが美しい。

・ライル＝オコナー
英国国家保安局の護衛官。三十代半ば。短髮黑髮の口髭が似合う男。

・ロブ＝ギャレット
英国国家保安局の護衛官。ライルの同期。

・天之河聖治せいじ
光輝の父。見た目は怜悧な超イケメンだが中身はチキン。優秀な經營コンサルタント。

・天之河美耶みや
光輝の母。元ヤンかつ總長で超美人。八重樫流の師範クラスに、金屬バット術で真っ向勝負できる武闘派。武術を習うより、素のスペックを活かした獨自の戰い方のほうが強いという規格外。放浪癖とトラブルホイホイ氣質がある。現在は人氣モデル雜誌の編集長。

・天之河美月みつき
始まりのソウルシスター。ソウルシスターズ創設者＆教祖。強い。

・アンノウン
書物に語られない正體不明の大惡魔。克勞蒂亞を母體に受肉して現世と地獄の支配を企んだが、深淵卿と聖女のタッグに敗北した。

・七柱の惡魔王
繆を守る大罪戰隊デモンレンジャーの中身。他にも有名どころが多々、ハジメのグリム達の中に入っている模樣。この事件の後、南雲家の魔王桑のところに、地獄の惡魔桑達から就職面接を望む聲が殺到した。

《郝里亞がやって來た》

・禍まがつ冥府ヘル・の羅刹蜃鬼ディザスター　カームライト・モルス・エクディキス・郝里亞
いつも絶好調なおっ桑。

・深淵アビス・正妻セニシエンタ　ラナインフェリナ・ブライド・郝里亞
奧義を發動すれば、ただの綺麗なお姉桑になれる。

・必滅狂亂デス・ラプソディア　バルトフェルド・ティライユール・郝里亞
樹海の廚二病王。廚二パンデミックの感染源はだいたいこいつ。

・外殺鏖華キル・ナハトール　ネアシュタットルム・アディカ・郝里亞
マジックテープという人類の英知の結晶により、一時的に正氣を取り戻して普通に可愛いウサミミ美少女になれる。

・空裂リーパー・ザ葬獄・インフェルノ　ミナステリア・ディアボルス・郝里亞
獨り身が辛い⋯

・雷刃のイオ（以下、略）

・ウサミミメイドさん
語尾がぴょん。ただし、１００ｍ１１秒台で走れるウサミミメイド。『神速の◯ンパルス』や『デビ◯バットゴースト』など數々の技能を有する。美少女をお持ち歸りしたい一心で、最近『デビル４◯ィメンション』も會得したとか。

《希雅召喚編》

・エリック＝ルクシード＝バルテッド
バルテッド王国の若き王。金獅子を彷彿とさせるイケメン。希雅に惚れ、魔王に戀心を打ち砕かれた。

・ルイス＝レクトール
バルテッド王国の筆頭宮廷靈法師。エリックの幼馴染み。鼻眼鏡をかけたインテリ系のイケメン。希雅を召喚した張本人。希雅に惚れ、魔王に戀心を打ち砕かれた。

・フィル＝エスピオン
バルテッド王国の諜報部隊隊長。エリックの幼馴染み。綠髮の輕薄そうなイケメン。希雅に惚れ、以下同文。

・グレッグ＝エクセスト
バルテッド王国の近衛騎士團團長。寡默なイケメン以下同文。

・ダリア＝シュワイク
バルテッド王国の公爵令孃。金髮金目、１８歳の美女。遙か昔の召喚者（日本人）により、女僕服が至高の衣裝だと思っている。兩手で小さな拳を作ってガッツポーズしつつ「～でございます！」と言うのが癖。エリックの幼馴染みで信頼の厚い臣下だったが、氣が付いたら某魔王と主從していた。エリックさんはわけが分からなかった。希雅と心を通わせたため、ハジメは二人が再會できるように對策を始めている。再會できる日も近い⋯⋯

・アロガン＝スペルビア＝レテッド
魔族の国、レテッド魔王国の国王。黑い長髮にアメジストの瞳、絶世の美貌を持ったイケメン。魔王（ヤバイ方）にピチュンとペカーッを繰り返され、文字通り死ぬほど力の差を分からされて大人しい魔王になった。

・グルウェル＝ドゥラーク＝シンテッド
獸人族の国、シンテッド獸王国の王。ドラゴンに變化できる點、竜人族に似ているが高潔とは程遠い日和見主義。魔王（ヤバイ方）に以下同文。

・アストルス＝フィン＝ホンテッド
天人族の国、ホンテッド天王国の王。ハジメにちょっとした嫌がらせを受けて無力化された。もう、紅色は見たくない⋯⋯

・ウダル
雷雲の神靈。嚴格そうな美丈夫の姿。希雅から理解不能な毆打を受けまくって心が折れる。あと、希雅に雷速を視認してから回避する進化をもたらした張本人。最終的に、もじょる黃色史萊姆になる。

・オロス
大地の神靈。巨大な土と岩石のゴーレムの姿。希雅から１００トンハンマーを食らい、ハジメからも砲擊を食らいまくって心が折れた。最終的に、もじょる土色史萊姆になる。

・ソアレ
火輪の神靈。意識高い系のキャリアウーマンみたいな雰圍氣だが、ハジメに太陽光集束雷射を食らい、ドパンッペカーッを繰り返されて心が折れた。すっかりヘタレるが、元來プライドが高いので虛勢を張りまくる。口癖は「や、やりますかっ！？　アァ！？　この──このがっ」。優しくしてくれる相手にはチョアレになる。病嬌氣質もあるので『病嬌駄チョレ』などと呼ばれることも。最終的に、もにょわ～と溶けたアイスみたいな赤色史萊姆になる。

・エンティ
流天の神靈。淡い綠色のツインテールと踴り子衣裝に身を包む十代半ばの少女の姿。ツンデレ。ハジメとの戰いの中、神靈なのに何度も尻叩きされて心が折れた。ハジメへの口癖は「もっと優しくしなさいよ！」「もっと褒めなさいよ！」など。最終的に、積極的にもじょる綠色史萊姆になる。

・メーレス
海流の神靈。巨大な海龍の姿。ハジメに何度も體內から爆破されて心が折れた。最終的に、それほどもじょらない水色史萊姆になる。

・バラフ
冰雪の神靈。クリスタルのような透明感の大鷲の姿。大量のグリム・グリフォンから集中砲火を受け續けて心が折れた。最終的に、わらび餅みたいな史萊姆になった。

・ライラ
常闇の神靈。黑のドレスと霧を纏った黑髮ロングの妙齢の美女。ハジメに亀甲縛りにされて振り回され續けたせいで心が折れた。最終的に、もじょ！とした黑色史萊姆になる。氣位の高さは變わらないが、最近はソアレに張り合って希雅の寵愛を得たい模樣。

・ルトリア
世界の中心にして要たる星樹の化身。純白と光に包まれた神聖な女性の姿。誕生して初めて、顏面パンチやら腹パンを食らって心が折れた。最終的に、ハジメ達の力で人の世から隔離してもらい、世界の行く末を見守ることになった。月樣が少々苦手。目を付けられてしまった⋯⋯怖い⋯⋯

《トータス旅行記》

・露露亞莉雅・Ｓ・Ｂ・海利希
海利希王国の王妃。莉莉安娜の母親。最近、少女漫畫に夢中。トータスにて『南乃スミレ作品』の劇場化を推し進めている。

・桑吉斯先生
蘭迪爾の教育係の老人。蘭迪爾の青春的葛藤を基本的に無視する。

・庫潔莉＝レイル
ブラックな職場でうつ病寸前の王国騎士團長。拳で自在に心臓を止めたり動かしたりできる。最近、真紅のブローチを眺めてはふにゃりとした笑顏を見せることが多々あり、それにより『團長中毒者』を増やしている。

・メタモルフォーゼ＝キャサリン
變身するキャサリン（冒険者公會の臨時事務長）

・アラベル
かつての金ランク冒険者『閃刃のアベル』の成れの果て⋯⋯ではなく進化形。

・ポーター＝ヘリー
布魯克の町の門番。門番という仕事の利點を世界に廣めた門番傳道師。

・布魯克の町の住人
月醬に踏まれ隊、希雅醬の奴隷になり隊、月お姉樣と姉妹になり隊、魔王の玉ぁ取り隊、コスプレイヤー、漢女、神出鬼沒の宿屋の娘、今にも死にそうな町長で住民の九割が構成されている。

・アダム＝ウォーカー
死にそうな布魯克の町の町長。キャサリンの旦那桑。奧桑がいないと、基本的に「あ、はい」しかしゃべらない。

・ジョナサン＝ウォーカー
キャサリンの息子。ハジメのコスプレイヤー。十四歳。

・イヴリン＝ウォーカー
キャサリンの娘。緹奧のコスプレイヤー。十一歳。

・馬薩卡一家
母キーナ、父ガラドリウス、むっつり娘索娜の三人家族。ご先祖に宿屋の看板娘なウサギがいた⋯⋯かもしれない。

・ベルファミリー（漢女一家）

惡夢。

・シモン・Ｌ・Ｇ・リベラール
新生聖教教會の教皇猊下。ひょうきんな爺だが、懐は深く、器はでかい。『放浪猊下』『失踪と疾走の達人』『指名手配され系教皇樣』など數々の異名を持つ。

・ミラクル＝アイ（にじゅうろくさい！）

幻の魔法少女。

≪その他≫
・バスガイドさん
強者つわもの。

・バスの運轉手桑
バスガイドを導く者。

・橋の上の妬ましい美女
繆の友達。

・英国の森の魔女桑
森を操る魔女。ハジメの平和的交渉を無視したので、いつも通りピチュンされた。テリトリーに大樹ウーア・アルトに似た痕跡がある。

・土井署長
表の顏はハジメが住む町の警察署署長。しかし、その實體は八重樫流の門下生。五十六歳だがフルマラソンを鼻歌交じり、かつ二時間半以內に走破できる猛者。

・山咲くん
同警察署の若手刑事。しかし、その實體は八重樫流の門下生。

・エガリさん／ノガリさん
アラクネの中身。どうも『神の使徒』エーアストくさい何かと、ノイントくさい何かが入っている模樣。なので、エーアスト（仮）、ノイント（仮）を略して呼稱することに。


════════════════════

いつもお讀みいただきありがとうございます。
感想・意見・誤字脫字報告もありがとうございます。

※コミックガルド、更新情報。
・１０／１１更新　零１７話
例の宿屋のウサギがちょろっと出てます。あと、さりげなく繆の友達になっている、クリオネモドキも。
・１０／４更新　日常４５話
今回は駄竜回。鈴と龍太郎も良いコンビ感ですｗ
・１０／４更新　本編３５話
希雅の戰闘シーンが充實。かっこ可愛い。

※アニメ２期製作決定の祝コメ、たく桑いただきありがとうございました！
これからもよろしくお願いします！